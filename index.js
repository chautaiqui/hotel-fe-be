const mongoose = require('mongoose');
require('dotenv').config();

const app = require('./src/app');

// connect to mongodb
const dbConfig = require('./src/config/db.config');
// const mongo_uri = `mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`;
const mongo_uri = process.env.MONGODB_URI;
mongoose
	.connect(mongo_uri, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true,
		retryWrites: false,
	})
	.then(() => {
		console.log('Successfully connect to MongoDB.');
	})
	.catch((err) => {
		console.error('Connection error', err);
		process.exit();
	});

// set port, listen for request
const PORT = process.env.PORT || 8000;
let server = app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});

// handle process 'uncaughtException' and 'unhandledRejection' error
const exitHandler = () => {
	if (server) {
		server.close(() => {
			// logger.info('Server closed');
			console.log('Server closed!');
			process.exit(1);
		});
	} else {
		process.exit(1);
	}
};

const unexpectedErrorHandler = (err) => {
	// logger.error(err);
	console.log(err);
	exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);
