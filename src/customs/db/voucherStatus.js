const voucherStatus = {
    AVAILABLE: 'available',
    UNAVAILABLE: 'unavailable',
};

const voucherStatusArr = ['available', 'unavailable'];

module.exports = {
    voucherStatus,
    voucherStatusArr
};