const accountStatus = {
    AVAILABLE: 'available',
    UNAVAILABLE: 'unavailable',
};

const accountStatusArr = ['available', 'unavailable'];

module.exports = {
    accountStatus,
    accountStatusArr
};