const documentStatus = {
    AVAILABLE: 'available',
    UNAVAILABLE: 'unavailable', //equivalent to be removed
};

const documentStatusArr = ['available', 'unavailable'];

module.exports = {
    documentStatus,
    documentStatusArr
};