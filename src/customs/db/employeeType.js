const employeeType = {
    EMPLOYEE: "employee",
    MANAGER: "manager",
    PARTTIME: "parttime",
};

const employeeTypeArr = ["employee", "manager", "parttime"];

module.exports = {
    employeeType,
    employeeTypeArr,
}