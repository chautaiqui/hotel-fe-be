const roomStatus = {
    AVAILABLE: "available",
    UNAVAILABLE: "unavailable",
    BOOKED: "booked",
};

const roomStatusArr = ["available", "unavailable", "booked"];

module.exports = {
    roomStatus,
    roomStatusArr,
};