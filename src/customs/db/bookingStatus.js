const bookingStatus = {
	PENDING: 'pending',
	COMPLETED: 'completed',
	CANCELLED: 'cancelled',
};

const bookingStatusArr = ['pending', 'completed', 'cancelled'];

module.exports = {
	bookingStatus,
	bookingStatusArr,
};
