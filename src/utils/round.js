const roundUp = (value) => {
	return Math.round(value / 10000) * 10000;
};

module.exports = { roundUp };
