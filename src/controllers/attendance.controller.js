const httpStatus = require('http-status');
// test
const catchAsync = require('../utils/catchAsync');
const {
	hotelShiftService,
	employeeService,
	attendanceService,
} = require('../services');

module.exports.takeAttendance = catchAsync(async (req, res, next) => {
	const hotelShift = await hotelShiftService.getHotelShiftById(
		req.params.hotel_shift_id
	);
	const emp = await employeeService.getEmployeeById(req.params.emp_id);
	req.body.shifts = hotelShift._id;
	req.body.employee = emp._id;
	const attendance = await attendanceService.takeAttendance(req.body);

	res.status(httpStatus.OK).send(attendance);
});

module.exports.queryManyAttendanceByHotelShiftId = catchAsync(
	async (req, res, next) => {
		const attendances =
			await attendanceService.queryManyAttendanceByHotelShiftId(
				{ shifts: req.params.id },
				{}
			);

		res.status(httpStatus.OK).send({ attendances });
	}
);
