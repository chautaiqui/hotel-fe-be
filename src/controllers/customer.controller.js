const {
	customerService,
	tokenService,
	cloudinaryService,
} = require('../services');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');

module.exports.createCustomer = catchAsync(async (req, res, next) => {
	const customer = await customerService.createCustomer(req.body);
	res.status(httpStatus.CREATED).send(customer);
});

module.exports.login = catchAsync(async (req, res, next) => {
	const { email, password } = req.body;
	const customer = await customerService.loginCustomerWithEmailAndPassword(
		email,
		password
	);
	const token = await tokenService.generateToken(customer._id);
	res.send({ customer, token });
});

module.exports.getAll = catchAsync(async (req, res, next) => {
	const customers = await customerService.getAll();
	res.status(httpStatus.OK).send({ customers });
});

module.exports.getCustomerById = catchAsync(async (req, res, next) => {
	const customer = await customerService.getCustomerById(req.params.id);
	res.send(customer);
});

module.exports.updateProfile = catchAsync(async (req, res, next) => {
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}
	const customer = await customerService.updateProfileById(
		req.params.id,
		req.body
	);
	res.send(customer);
});

module.exports.getVouchersByCustomerId = catchAsync(async (req, res, next) => {
	const vouchers = await customerService.getVouchersByCustomerId(
		req.params.id
	);
	res.status(httpStatus.OK).send({ vouchers });
});

module.exports.getBookingsByCustomerId = catchAsync(async (req, res, next) => {
	const bookings = await customerService.getBookingsByCustomerId(
		req.params.id
	);
	res.status(httpStatus.OK).send({ bookings });
});
