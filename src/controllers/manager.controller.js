const {
	managerService,
	hotelService,
	tokenService,
	cloudinaryService,
} = require('../services');
const { employeeType } = require('../customs/db/employeeType');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');

module.exports.createManager = catchAsync(async (req, res, next) => {
	req.body.type = employeeType.MANAGER;
	const manager = await managerService.createManager(req.body);
	res.status(httpStatus.CREATED).send(manager);
});

module.exports.login = catchAsync(async (req, res, next) => {
	const { email, password } = req.body;
	const manager = await managerService.loginManagerWithEmailAndPassword(
		email,
		password
	);
	const token = await tokenService.generateToken(manager._id);
	res.status(httpStatus.OK).send({ manager, token });
});

module.exports.getAllManager = catchAsync(async (req, res, next) => {
	const managers = await managerService.getManagersByFilter({
		type: employeeType.MANAGER,
	});
	res.status(httpStatus.OK).send({ managers });
});

module.exports.getManagerById = catchAsync(async (req, res, next) => {
	const manager = await managerService.getManagerById(req.params.id);
	res.status(httpStatus.OK).send(manager);
});

module.exports.updateManagerById = catchAsync(async (req, res, next) => {
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}
	const manager = await managerService.updateProfileById(
		req.params.id,
		req.body
	);
	res.status(httpStatus.OK).send(manager);
});
