const { blogService, cloudinaryService } = require('../services');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

module.exports.createBlog = catchAsync(async (req, res, next) => {
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}

	const blog = await blogService.createBlog(req.body);
	res.status(httpStatus.CREATED).send(blog);
});

module.exports.getAllBlog = catchAsync(async (req, res, next) => {
	const { title } = req.query;
	const blogs = await blogService.getAllBlog(title);
	res.status(httpStatus.OK).send(blogs);
});

module.exports.getBlogById = catchAsync(async (req, res, next) => {
	const blog = await blogService.getBlogById(req.params.id);
	res.status(httpStatus.OK).send(blog);
});

module.exports.updateBlogById = catchAsync(async (req, res, next) => {
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}

	const blog = await blogService.updateBlogById(req.params.id, req.body);
	res.status(httpStatus.OK).send(blog);
});

module.exports.deleteBlogById = catchAsync(async (req, res, next) => {
	const blog = blogService.deleteBlogById(req.params.id);
	res.status(httpStatus.OK).send({});
});
