const {
	roomService,
	facilityService,
	hotelService,
	roomTypeService,
} = require('../services');
const httpStatus = require('http-status');
const ApiEror = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

exports.createRoom = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	req.body.hotel = hotel._id;
	const roomType = await roomTypeService.getRoomTypeByNameAndHotelId(
		req.body.roomType,
		hotel._id
	);
	req.body.roomType = roomType._id;

	if (req.body.facilities) {
		const facilities = await facilityService.getFacilitiesForRoom(
			req.body.facilities,
			hotel._id
		);
		req.body.facilities = facilities;
	}

	const room = await roomService.createRoom(req.body);
	res.status(httpStatus.CREATED).send(room);
});

exports.getAllRoomByHotelId = catchAsync(async (req, res, next) => {
	const rooms = await roomService.getAllRoomByHotelId(req.params.id);
	res.status(httpStatus.OK).send({ rooms });
});

exports.getRoomById = catchAsync(async (req, res, next) => {
	const room = await roomService.getRoomById(req.params.id);
	res.status(httpStatus.OK).send(room);
});

exports.updateRoomById = catchAsync(async (req, res, next) => {
	const room = await roomService.getRoomById(req.params.id);

	if (req.body.roomType) {
		const roomType = await roomTypeService.getRoomTypeByNameAndHotelId(
			req.body.roomType,
			room.hotel
		);
		req.body.roomType = roomType._id;
	}

	if (req.body.facilities) {
		await facilityService.removeFacilitiesForRoom(
			room.facilities,
			room.hotel
		);

		const facilities = await facilityService.getFacilitiesForRoom(
			req.body.facilities,
			room.hotel
		);
		req.body.facilities = facilities;
	}

	const roomUpdated = await roomService.updateRoomById(
		req.params.id,
		req.body
	);
	res.status(httpStatus.OK).send(roomUpdated);
});
