const httpStatus = require('http-status');

const { hotelShiftService, hotelService } = require('../services');
const ApiEror = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

module.exports.createHotelShift = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	req.body.hotel = hotel._id;
	const hotelShift = await hotelShiftService.createHotelShift(req.body);
	res.status(httpStatus.CREATED).send(hotelShift);
});

module.exports.getAllHotelShiftByHotelId = catchAsync(
	async (req, res, next) => {
		const hotelShifts = await hotelShiftService.queryManyHotelShift(
			{ hotel: req.params.id },
			{}
		);
		res.status(httpStatus.OK).send({ hotelShifts });
	}
);

module.exports.getHotelShiftById = catchAsync(async (req, res, next) => {
	const hotelShift = await hotelShiftService.getHotelShiftById(req.params.id);
	res.status(httpStatus.OK).send(hotelShift);
});

module.exports.addEmpToHotelShift = catchAsync(async (req, res, next) => {
	const employeeList = await hotelShiftService.addEmpToHotelShift(
		req.params.hotelshift_id,
		req.params.emp_id
	);

	res.status(httpStatus.OK).send(employeeList);
});

module.exports.viewEmployeeInShift = catchAsync(async (req, res, next) => {
	const employees = await hotelShiftService.viewEmployeeInShift(
		req.params.id
	);

	res.status(httpStatus.OK).send(employees);
});
