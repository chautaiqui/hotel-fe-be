const { ratingService, hotelService, customerService } = require('../services');
const httpStatus = require('http-status');
const ApiEror = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

exports.rating = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	const customer = await customerService.getCustomerById(req.body.customer);
	const rating = await ratingService.rating(
		hotel._id,
		customer._id,
		req.body.rating,
		req.body.comment ? req.body.comment : ''
	);
	res.status(httpStatus.OK).send(rating);
});

exports.getRatings = catchAsync(async (req, res, next) => {
	const ratings = await ratingService.getRatings(req.params.id);
	res.status(httpStatus.OK).send({ ratings });
});
