const {
	hotelService,
	roomTypeService,
	cloudinaryService,
} = require('../services');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');
const { add } = require('lodash');

module.exports.createHotel = catchAsync(async (req, res, next) => {
	if (req.files) {
		req.body.imgs = await cloudinaryService.uploadManyImg(req.files);
	}
	const hotel = await hotelService.createHotel(req.body);
	res.status(httpStatus.CREATED).send(hotel);
});

module.exports.getAllHotel = catchAsync(async (req, res, next) => {
	const hotels = await hotelService.getAllHotel();
	res.status(httpStatus.OK).send({ hotels });
});

module.exports.getHotels = catchAsync(async (req, res, next) => {
	const query = req.query;

	if (query.manager == false) {
		const hotels = await hotelService.getHotelsWithoutManager(query);
		res.status(httpStatus.OK).send(hotels);
	} else {
		delete query.manager;
		const hotels = await hotelService.getHotels(query, {});
		res.status(httpStatus.OK).send(hotels);
	}
});

module.exports.getHotelById = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	res.status(httpStatus.OK).send(hotel);
});

module.exports.updateHotelById = catchAsync(async (req, res, next) => {
	if (req.files) {
		req.body.files = await cloudinaryService.uploadManyImg(req.files);
	}
	const hotel = await hotelService.updateHotelById(req.params.id, req.body);
	res.status(httpStatus.OK).send(hotel);
});

module.exports.changeManager = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.changeManager(
		req.params.id,
		req.body.manager
	);
	res.status(httpStatus.OK).send(hotel);
});

module.exports.getHotelsWithoutManager = catchAsync(async (req, res, next) => {
	const hotels = await hotelService.getHotelsWithoutManager();
	res.status(httpStatus.OK).send({ hotels });
});

module.exports.confirmLeave = catchAsync(async (req, res, next) => {
	const employee = await hotelService.confirmLeave(req.params.id);
	res.status(httpStatus.OK).send(employee);
});

module.exports.getLeaveByHotelId = catchAsync(async (req, res, next) => {
	const leave = await hotelService.getLeaveByHotelId(req.params.id);
	res.status(httpStatus.OK).send(leave);
});

module.exports.getLeaveBySpecificTime = catchAsync(async (req, res, next) => {
	const leave = await hotelService.getLeaveBySpecificTime(
		req.params.id,
		req.body.month,
		req.body.year
	);
	res.status(httpStatus.OK).send(leave);
});
