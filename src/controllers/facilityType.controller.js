const { facilityTypeService, facilityService, hotelService } = require("../services");
const httpStatus = require("http-status");
const ApiEror = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");

module.exports.createFacilityType = catchAsync(async (req, res, next) => {
    const hotel = await hotelService.getHotelById(req.params.id);
    req.body.hotel = hotel._id;
    const facilityType = await facilityTypeService.createFacilityType(req.body);
    res.status(httpStatus.CREATED).send(facilityType);
});

module.exports.getAllFacilityTypeByHotelId = catchAsync(async (req, res, next) => {
    const hotel = await hotelService.getHotelById(req.params.id);
    const facilityTypes = await facilityTypeService.getAllFacilityTypeByHotelId(hotel._id);

    // get all facilities of each facility type
    types = await Promise.all(facilityTypes.map(async (facilityType) => {
        const facilities = await facilityService.queryManyFacility({type: facilityType._id}, {});
        const type = JSON.parse(JSON.stringify(facilityType));
        type.facilities = facilities;
        return type;
    }));

    res.status(httpStatus.OK).send({facilititypes: types});
    // res.status(httpStatus.OK).send(types);
});

module.exports.getFacilityTypeById = catchAsync(async (req, res, next) => {
    const facilityType = await facilityTypeService.getFacilityTypeById(req.params.id);
    const facilities = await facilityService.queryManyFacility({type: facilityType._id}, {});
    const type = JSON.parse(JSON.stringify(facilityType));
    type.facilities = facilities;
    res.status(httpStatus.OK).send(type);
});

// update facilityType by facilityTypeId
module.exports.updateFacilityTypeById = catchAsync(async (req, res, next) => {
    const facilityType = await facilityTypeService.updateFacilityTypeById(req.params.id, req.body);
    res.status(httpStatus.OK).send(facilityType);
});