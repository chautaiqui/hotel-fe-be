const { roomTypeService, hotelService } = require('../services');
const httpStatus = require('http-status');
const ApiEror = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

module.exports.createRoomType = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	req.body.hotel = hotel._id;
	const roomType = await roomTypeService.createRoomType(req.body);
	res.status(httpStatus.CREATED).send(roomType);
});

module.exports.getAllRoomTypeByHotelId = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	const roomTypes = await roomTypeService.getAllRoomTypeByHotelId(hotel._id);
	res.status(httpStatus.OK).send({ roomTypes });
});

module.exports.getRoomTypeById = catchAsync(async (req, res, next) => {
	const roomType = await roomTypeService.getRoomTypeById(req.params.id);
	res.status(httpStatus.OK).send(roomType);
});

// update roomType by roomTypeId
module.exports.updateRoomTypeById = catchAsync(async (req, res, next) => {
	const roomType = await roomTypeService.updateRoomTypeById(
		req.params.id,
		req.body
	);
	res.status(httpStatus.OK).send(roomType);
});
