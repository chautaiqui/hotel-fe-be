const { bookingReviewService } = require('../services');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

exports.createBookingReview = catchAsync(async (req, res, next) => {
	const review = await bookingReviewService.createReview(
		req.params.id,
		req.body
	);
	res.status(httpStatus.CREATED).send({ review });
});

exports.findById = catchAsync(async (req, res, next) => {
	const review = await bookingReviewService.findById(req.params.id);
	res.status(httpStatus.OK).send({ review });
});

exports.findByHotelId = catchAsync(async (req, res, next) => {
	const reviews = await bookingReviewService.findByHotelId(req.params.id);
	res.status(httpStatus.OK).send({ reviews });
});

exports.updateById = catchAsync(async (req, res, next) => {
	const review = await bookingReviewService.updateById(
		req.params.id,
		req.body
	);
	res.status(httpStatus.OK).send({ review });
});
