const {
	voucherService,
	hotelService,
	roomService,
	customerService,
	bookingService,
} = require('../services');
const { voucherStatus } = require('../customs/db/voucherStatus');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

exports.createBooking = catchAsync(async (req, res, next) => {
	const room = await roomService.getRoomById(req.body.room);
	Object.assign(req.body, {
		hotel: room.hotel,
		room: room._id,
	});
	const booking = await bookingService.createBooking(req.body);
	res.status(httpStatus.CREATED).send(booking);
});

exports.paymentBooking = catchAsync(async (req, res, next) => {
	const booking = await bookingService.paymentBooking(req.params.id);
	res.status(httpStatus.OK).send(booking);
});

exports.getBookingById = catchAsync(async (req, res, next) => {
	const booking = await bookingService.getBookingById(req.params.id);
	res.status(httpStatus.OK).send(booking);
});

// exports.updateBooking = catchAsync(async (req, res, next) => {
// 	const booking = await bookingService.updateBookingById(
// 		req.params.id,
// 		req.body
// 	);
// 	res.status(httpStatus.OK).send(booking);
// });

exports.cancelBooking = catchAsync(async (req, res, next) => {
	const booking = await bookingService.cancelBookingById(req.params.id);
	res.status(httpStatus.OK).send({
		message: 'Booking was canceled successfully.',
	});
});

exports.getBookingsByHotelId = catchAsync(async (req, res, next) => {
	const bookings = await bookingService.getBookingsByHotelId(req.params.id);
	res.status(httpStatus.OK).send({ bookings });
});
