const {
	employeeService,
	hotelService,
	tokenService,
	cloudinaryService,
	hotelShiftService,
} = require('../services');
const { employeeType } = require('../customs/db/employeeType');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');

module.exports.createEmployee = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	req.body.hotel = hotel._id;
	req.body.type = employeeType.EMPLOYEE;
	const employee = await employeeService.createEmployee(req.body);
	res.status(httpStatus.CREATED).send(employee);
});

module.exports.login = catchAsync(async (req, res, next) => {
	const { email, password } = req.body;
	const employee = await employeeService.loginEmployeeWithEmailAndPassword(
		email,
		password
	);
	const token = await tokenService.generateToken(employee._id);
	res.send({ employee, token });
});

module.exports.getAllEmployeeByHotelId = catchAsync(async (req, res, next) => {
	const hotel = await hotelService.getHotelById(req.params.id);
	const employees = await employeeService.getEmployeesByFilter({
		hotel: hotel._id,
		type: employeeType.EMPLOYEE,
	});
	res.status(httpStatus.OK).send({ employees });
});

module.exports.getEmployeeById = catchAsync(async (req, res, next) => {
	const employee = await employeeService.getEmployeeById(req.params.id);
	res.send(employee);
});

module.exports.updateEmployeeById = catchAsync(async (req, res, next) => {
	if (req.file) {
		req.body.img = await cloudinaryService.uploadImg(req.file);
	}
	const employee = await employeeService.updateProfileById(
		req.params.id,
		req.body
	);
	res.send(employee);
});

module.exports.getAllShiftByEmpId = catchAsync(async (req, res, next) => {
	const shifts = await employeeService.getAllShiftByEmpId(req.params.id);

	res.status(httpStatus.OK).send(shifts);
});

module.exports.getAllAttendanceByEmpId = catchAsync(async (req, res, next) => {
	const attendances = await employeeService.getAllAttendanceByEmpId(
		req.params.id
	);

	res.status(httpStatus.OK).send(attendances);
});

module.exports.getSalaryByEmpId = catchAsync(async (req, res, next) => {
	const salary = await employeeService.getSalaryByEmpId(
		req.body.month,
		req.body.year,
		req.params.id
	);

	res.status(httpStatus.OK).send({ salary });
});

module.exports.getAttendanceByMonth = catchAsync(async (req, res, next) => {
	const attendance = await employeeService.getAttendanceByMonth(
		req.body.month,
		req.body.year,
		req.params.id
	);

	res.status(httpStatus.OK).send({ attendance });
});

module.exports.getShiftBySpecificTime = catchAsync(async (req, res, next) => {
	const shift = await employeeService.getShiftBySpecificTime(
		req.body.month,
		req.body.year,
		req.params.id
	);
	res.status(httpStatus.OK).send({ shift });
});

module.exports.applyForLeave = catchAsync(async (req, res, next) => {
	const shift = await hotelShiftService.getHotelShiftById(
		req.params.shift_id
	);
	const employee = await employeeService.getEmployeeById(req.params.emp_id);
	const hotel = await hotelService.getHotelById(req.params.hotel_id);
	req.body.hotel = hotel._id;
	req.body.employee = employee._id;
	req.body.hotelShift = shift._id;
	const leave = await employeeService.applyForLeave(req.body);

	res.status(httpStatus.CREATED).send(leave);
});

module.exports.getLeaveByEmpId = catchAsync(async (req, res, next) => {
	const leave = await employeeService.getLeaveByEmp(req.params.id);
	return leave;
});
