const { hotelReportService } = require('../services');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');

exports.getReport = catchAsync(async (req, res, next) => {
	const hotelId = req.params.id;
	const { month, year } = req.query;
	const hotelReport = await hotelReportService.getReport(
		hotelId,
		month,
		year
	);
	res.status(httpStatus.OK).send(hotelReport);
});

exports.getAdminReport = catchAsync(async (req, res, next) => {
	const { month, year } = req.query;
	const hotelAdminReport = await hotelReportService.getAdminReport(
		month,
		year
	);
	res.status(httpStatus.OK).send(hotelAdminReport);
});
