const { authService, tokenService } = require('../services')
const httpStatus = require('http-status')
const catchAsync = require('../utils/catchAsync')
const ApiError = require('../utils/ApiError')

exports.login = catchAsync(async (req, res, next) => {
    const { email, password } = req.body
    const { auth, role } = await authService.login(email, password)
    const token = await tokenService.generateToken(auth._id)
    res.status(httpStatus.OK).send({ auth, role, token })
})
