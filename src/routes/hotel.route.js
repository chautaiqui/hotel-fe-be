const express = require('express');
const validate = require('../middlewares/validate');
const emailValidation = require('../middlewares/emailValidation');

const {
	hotelController,
	employeeController,
	roomTypeController,
	roomController,
	facilityTypeController,
	facilityController,
	ratingController,
	voucherController,
	hotelShiftController,
	bookingController,
	hotelReportController,
} = require('../controllers');
const {
	hotelValidation,
	employeeValidation,
	roomTypeValidation,
	roomValidation,
	facilityTypeValidation,
	facilityValidation,
	ratingValidation,
	voucherValidation,
	hotelShiftValidation,
	bookingValidation,
	hotelReportValidation,
} = require('../validations');

const multer = require('multer');
const fileUpload = multer();

const router = express.Router();

// hotel shift router
const hotelShiftRouter = express.Router();

//manager create shift by hotel id
hotelShiftRouter
	.route('/:id/create-hotel-shift')
	.post(
		validate(hotelShiftValidation.createHotelShift),
		hotelShiftController.createHotelShift
	);

// get all hotel shift by id of hotel
hotelShiftRouter
	.route('/:id/hotel-shifts')
	.get(
		validate(hotelShiftValidation.getAllHotelShiftByHotelId),
		hotelShiftController.getAllHotelShiftByHotelId
	);

// employee router
const employeeRouter = express.Router();
employeeRouter
	.route('/:id/create-employee')
	.post(
		validate(employeeValidation.createEmployee),
		emailValidation,
		employeeController.createEmployee
	);
employeeRouter
	.route('/:id/employee')
	.get(
		validate(employeeValidation.getAllEmployeeByHotelId),
		employeeController.getAllEmployeeByHotelId
	);
//

// room type router
const roomTypeRouter = express.Router();
roomTypeRouter
	.route('/:id/roomtype')
	.get(
		validate(roomTypeValidation.getAllRoomTypeByHotelId),
		roomTypeController.getAllRoomTypeByHotelId
	);
roomTypeRouter
	.route('/:id/create-roomtype')
	.post(
		validate(roomTypeValidation.createRoomType),
		roomTypeController.createRoomType
	);

// room router
const roomRouter = express.Router();
roomRouter
	.route('/:id/room')
	.get(
		validate(roomValidation.getAllRoomByHotelId),
		roomController.getAllRoomByHotelId
	);
roomRouter
	.route('/:id/create-room')
	.post(validate(roomValidation.createRoom), roomController.createRoom);
//

// facility type router
const facilityTypeRouter = express.Router();
facilityTypeRouter
	.route('/:id/facilitytype')
	.get(
		validate(facilityTypeValidation.getAllFacilityTypeByHotelId),
		facilityTypeController.getAllFacilityTypeByHotelId
	);
facilityTypeRouter
	.route('/:id/create-facilitytype')
	.post(
		validate(facilityTypeValidation.createFacilityType),
		facilityTypeController.createFacilityType
	);
//

// facility router
const facilityRouter = express.Router();
facilityRouter
	.route('/:id/create-facility')
	.post(
		validate(facilityValidation.createFacility),
		facilityController.createFacility
	);
facilityRouter
	.route('/:id/facility')
	.get(
		validate(facilityValidation.getAllFacilityByHotelId),
		facilityController.getAllFacilityByHotelId
	);
//

// rating router
const ratingRouter = express.Router();
ratingRouter
	.route('/:id/rating')
	.post(validate(ratingValidation.rating), ratingController.rating)
	.get(validate(ratingValidation.getRatings), ratingController.getRatings);
//

// voucher router
const voucherRouter = express.Router();
voucherRouter
	.route('/:id/create-voucher')
	.post(
		fileUpload.single('img'),
		validate(voucherValidation.createVoucher),
		voucherController.createVoucher
	);
voucherRouter
	.route('/:id/voucher')
	.get(
		validate(voucherValidation.getVouchersByHotelId),
		voucherController.getVouchersByHotelId
	);
//

// booking
const bookingRouter = express.Router();
bookingRouter
	.route('/:id/booking')
	.get(
		validate(bookingValidation.getBookingsByHotelId),
		bookingController.getBookingsByHotelId
	);

// router.route('/').get(hotelController.getAllHotel);
router
	.route('/')
	.get(validate(hotelValidation.getHotels), hotelController.getHotels);

router
	.route('/create')
	.post(
		fileUpload.array('imgs'),
		validate(hotelValidation.createHotel),
		hotelController.createHotel
	);

// Leave
const leaveRouter = express.Router();
leaveRouter
	.route('/:id/confirm-leave')
	.post(validate(hotelValidation.confirmLeave), hotelController.confirmLeave);

leaveRouter
	.route('/leave-form/:id')
	.get(
		validate(hotelValidation.getLeaveByHotelId),
		hotelController.getLeaveByHotelId
	);

leaveRouter
	.route('/leave-by-time/:id')
	.post(
		validate(hotelValidation.getLeaveBySpecificTime),
		hotelController.getLeaveBySpecificTime
	);

router.use(employeeRouter);
router.use(roomTypeRouter);
router.use(roomRouter);
router.use(facilityTypeRouter);
router.use(facilityRouter);
router.use(hotelShiftRouter);
router.use(ratingRouter);
router.use(voucherRouter);
router.use(leaveRouter);
router.use(bookingRouter);

router
	.route('/:id/report')
	.get(
		validate(hotelReportValidation.getHotelReport),
		hotelReportController.getReport
	);

router
	.route('/:id/change-manager')
	.put(
		validate(hotelValidation.changeManager),
		hotelController.changeManager
	);

router
	.route('/:id')
	.get(validate(hotelValidation.getHotelById), hotelController.getHotelById)
	.put(
		fileUpload.array('imgs'),
		validate(hotelValidation.updateHotelById),
		hotelController.updateHotelById
	);

module.exports = router;
