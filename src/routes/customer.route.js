const express = require('express');
const validate = require('../middlewares/validate');
const emailValidation = require('../middlewares/emailValidation');

const { customerController, voucherController } = require('../controllers');
const { customerValidation, voucherValidation } = require('../validations');

const multer = require('multer');
const fileUpload = multer();

const router = express.Router();

router.route('/').get(customerController.getAll);

router
	.route('/signup')
	.post(
		validate(customerValidation.createCustomer),
		emailValidation,
		customerController.createCustomer
	);

router
	.route('/login')
	.post(validate(customerValidation.login), customerController.login);

router
	.route('/:id/voucher')
	.get(
		validate(customerValidation.getVouchersByCustomerId),
		customerController.getVouchersByCustomerId
	);

router
	.route('/:id/booking')
	.get(
		validate(customerValidation.getBookingsByCustomerId),
		customerController.getBookingsByCustomerId
	);

router
	.route('/:id')
	.get(
		validate(customerValidation.getCustomerById),
		customerController.getCustomerById
	)
	.put(
		fileUpload.single('img'),
		validate(customerValidation.updateCustomerById),
		customerController.updateProfile
	);

module.exports = router;
