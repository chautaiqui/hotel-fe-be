const express = require("express");
const validate = require("../middlewares/validate");

const { facilityTypeController } = require("../controllers");
const { facilityTypeValidation } = require("../validations");

const router = express.Router();

router.route("/:id")
    .get(validate(facilityTypeValidation.getFacilityTypeById), facilityTypeController.getFacilityTypeById)
    .put(validate(facilityTypeValidation.updateFacilityTypeById), facilityTypeController.updateFacilityTypeById);


module.exports = router;