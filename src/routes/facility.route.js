const express = require("express");
const validate = require("../middlewares/validate");

const { facilityController } = require("../controllers");
const { facilityValidation } = require("../validations");

const router = express.Router();

router
  .route("/:id")
  .get(
    validate(facilityValidation.getFacilityById),
    facilityController.getFacilityById
  )
  .put(
    validate(facilityValidation.updateFacilityById),
    facilityController.updateFacilityById
  );

module.exports = router;
