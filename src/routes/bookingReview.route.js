const express = require('express');
const validate = require('../middlewares/validate');

const { bookingReviewController } = require('../controllers');
const { bookingReviewValidation } = require('../validations');

const router = express.Router();

router
	.route('/booking/:id')
	.post(
		validate(bookingReviewValidation.createReview),
		bookingReviewController.createBookingReview
	);

router
	.route('/hotel/:id')
	.get(
		validate(bookingReviewValidation.findByHotelId),
		bookingReviewController.findByHotelId
	);

router
	.route('/:id')
	.get(
		validate(bookingReviewValidation.findById),
		bookingReviewController.findById
	)
	.put(
		validate(bookingReviewValidation.updateById),
		bookingReviewController.updateById
	);

module.exports = router;
