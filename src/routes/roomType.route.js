const express = require("express");
const validate = require("../middlewares/validate");

const { roomTypeController } = require("../controllers");
const { roomTypeValidation } = require("../validations");

const router = express.Router();

router.route("/:id")
    .get(validate(roomTypeValidation.getRoomTypeById), roomTypeController.getRoomTypeById)
    .put(validate(roomTypeValidation.updateRoomTypeById), roomTypeController.updateRoomTypeById);


module.exports = router;