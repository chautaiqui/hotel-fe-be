const express = require('express');
const validate = require('../middlewares/validate');

const { bookingController } = require('../controllers');
const { bookingValidation } = require('../validations');

const router = express.Router();

router
	.route('/create')
	.post(
		validate(bookingValidation.createBooking),
		bookingController.createBooking
	);

router
	.route('/:id/payment')
	.post(
		validate(bookingValidation.paymentBooking),
		bookingController.paymentBooking
	);

router
	.route('/:id/cancel')
	.put(
		validate(bookingValidation.cancelBookingById),
		bookingController.cancelBooking
	);

router
	.route('/:id')
	.get(
		validate(bookingValidation.getBookingById),
		bookingController.getBookingById
	);

module.exports = router;
