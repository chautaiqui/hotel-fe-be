const express = require('express');
const validate = require('../middlewares/validate');

const { voucherController } = require('../controllers');
const { voucherValidation } = require('../validations');

const multer = require('multer');
const fileUpload = multer();

const router = express.Router();

// router.get(
// 	'/cron',
// 	validate(voucherValidation.simpleCronjob),
// 	voucherController.simpleCronjob
// );

router.get('/', voucherController.getAllVoucher);

router.get('/available', voucherController.getAllVoucher);

router
	.route('/:id/get')
	.post(
		validate(voucherValidation.customerGetVoucher),
		voucherController.customerGetVoucher
	);

router
	.route('/:id')
	.get(
		validate(voucherValidation.getVoucherById),
		voucherController.getVoucherById
	)
	.put(
		fileUpload.single('img'),
		validate(voucherValidation.updateVoucherById),
		voucherController.updateVoucherById
	);

module.exports = router;
