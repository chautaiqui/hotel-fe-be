const express = require('express');

const { attendanceController } = require('../controllers');
const validate = require('../middlewares/validate');
const { attendanceValidation } = require('../validations');

const router = express.Router();

router.route('/:hotel_shift_id/:emp_id').post(
	// validate(attendanceValidation.takeAttendance),
	attendanceController.takeAttendance
);

router
	.route('/view-attendances/:id')
	.get(
		validate(attendanceValidation.queryManyAttendanceByHotelShiftId),
		attendanceController.queryManyAttendanceByHotelShiftId
	);

module.exports = router;
