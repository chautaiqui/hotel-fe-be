const express = require('express');
const validate = require('../middlewares/validate');

const { employeeController } = require('../controllers');
const { employeeValidation } = require('../validations');

const multer = require('multer');
const fileUpload = multer();

const router = express.Router();

router
	.route('/login')
	.post(validate(employeeValidation.login), employeeController.login);

router
	.route('/:id')
	.get(
		validate(employeeValidation.getEmployeeById),
		employeeController.getEmployeeById
	)
	.put(
		fileUpload.single('img'),
		validate(employeeValidation.updateEmployeeById),
		employeeController.updateEmployeeById
	);

router
	.route('/:id/shifts')
	.get(
		validate(employeeValidation.getAllShiftByEmpId),
		employeeController.getAllShiftByEmpId
	);

router
	.route('/:id/view-attendances')
	.get(
		validate(employeeValidation.getAllAttendanceByEmpId),
		employeeController.getAllAttendanceByEmpId
	);

router
	.route('/:id/view-salary')
	.post(
		validate(employeeValidation.getSalaryByEmpId),
		employeeController.getSalaryByEmpId
	);

router
	.route('/:id/attendance-by-month-year')
	.post(
		validate(employeeValidation.getAttendanceByMonth),
		employeeController.getAttendanceByMonth
	);

router
	.route('/:id/shift-by-specific-time')
	.post(
		validate(employeeValidation.getShiftBySpecificTime),
		employeeController.getShiftBySpecificTime
	);

router
	.route('/:shift_id/:emp_id/:hotel_id/apply-leave')
	.post(
		validate(employeeValidation.applyForLeave),
		employeeController.applyForLeave
	);

router
	.route('/:id/view-leave')
	.get(
		validate(employeeValidation.getLeaveByEmpId),
		employeeController.getLeaveByEmpId
	);
module.exports = router;
