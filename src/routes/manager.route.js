const express = require('express');
const validate = require('../middlewares/validate');
const emailValidation = require('../middlewares/emailValidation');

const { managerController } = require('../controllers');
const { managerValidation } = require('../validations');

const multer = require('multer');
const fileUpload = multer();

const router = express.Router();

router.get('/', managerController.getAllManager);

router.post(
	'/create',
	validate(managerValidation.createManager),
	emailValidation,
	managerController.createManager
);

router
	.route('/login')
	.post(validate(managerValidation.login), managerController.login);

router
	.route('/:id')
	.get(
		validate(managerValidation.getManagerById),
		managerController.getManagerById
	)
	.put(
		fileUpload.single('img'),
		validate(managerValidation.updateProfileById),
		managerController.updateManagerById
	);

module.exports = router;
