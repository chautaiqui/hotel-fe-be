const express = require('express');
const validate = require('../middlewares/validate');
const emailValidation = require('../middlewares/emailValidation');

const { adminController, hotelReportController } = require('../controllers');
const { adminValidation, hotelReportValidation } = require('../validations');

const multer = require('multer');
const fileUpload = multer();

const router = express.Router();

router
	.route('/create')
	.post(
		validate(adminValidation.createAdmin),
		emailValidation,
		adminController.createAdmin
	);
router
	.route('/report')
	.get(
		validate(hotelReportValidation.getAdminReport),
		hotelReportController.getAdminReport
	);

router
	.route('/login')
	.post(validate(adminValidation.login), adminController.login);

router
	.route('/:id')
	.get(validate(adminValidation.getAdminById), adminController.getAdminById)
	.put(
		fileUpload.single('img'),
		validate(adminValidation.updateAdminById),
		adminController.updateProfile
	);

// router.route("/:id/change-password")
//     .put(adminController)

module.exports = router;
