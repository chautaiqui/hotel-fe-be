const express = require("express");
const validate = require("../middlewares/validate");

const { roomController } = require("../controllers");
const { roomValidation } = require("../validations");

const router = express.Router();

router.route("/:id")
    .get(validate(roomValidation.getRoomById), roomController.getRoomById)
    .put(validate(roomValidation.updateRoomById), roomController.updateRoomById);

module.exports = router;