const httpStatus = require('http-status');
const ApiError = require("../utils/ApiError");
const mongoose = require('mongoose');
const fs = require("fs");

// conver error to ApiError
const errorConverter = (err, req, res, next) => {
    let apiErr = err;
    if (!(apiErr instanceof ApiError)) {
        // const statusCode =
        //     apiErr.statusCode || apiErr instanceof mongoose.Error ? httpStatus.BAD_REQUEST : httpStatus.INTERNAL_SERVER_ERROR;
        const statusCode = 
            apiErr.statusCode || apiErr.name ==='MongoError' ? httpStatus.BAD_REQUEST : httpStatus.INTERNAL_SERVER_ERROR;
        const message = apiErr.message || httpStatus[statusCode];
        apiErr = new ApiError(statusCode, message, false, err.stack);
    }
    next(apiErr);
};


// handle error from catchAsync
const errorHandler = async (apiErr , req, res, next) => {
    let {statusCode, message, stack} = apiErr;
    const response = {
        code: statusCode,
        message,
        stack: stack
    }
    res.status(statusCode).send(response);
};


module.exports = {
    errorConverter,
    errorHandler
}