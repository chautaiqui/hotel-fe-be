const {Admin, Employee, Customer} = require("../models");
const ApiError = require("../utils/ApiError");
const httpStatus = require("http-status");
const validator = require("validator");

module.exports = async (req, res, next) => {
    const email = req.body.email;

    if (!validator.isEmail(email)) {
        const err = new ApiError(httpStatus.BAD_REQUEST, "Invalid email!");
        next(err);
    }

    // check email is already exist
    const admins  = await Admin.countDocuments({ email: email });
    const employees = await Employee.countDocuments({  email: email });
    const customers = await Customer.countDocuments({ email: email });
    if (admins > 0 || employees > 0 || customers > 0) {
        const err = new ApiError(httpStatus.BAD_REQUEST, "Email is already in use.");
        next(err);
    }

    next();
}