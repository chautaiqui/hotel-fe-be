// set your env variable CLOUDINARY_URL or set the following configuration
// module.exports = {
//     cloud_name: 'hotellv',
//     api_key: '715279966759186',
//     api_secret: 'A_pAIcJZRH29JKA1cnFQDTLAoyg'
// };

module.exports = {
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
};