const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");

const createBooking = {
  body: Joi.object().keys({
    room: Joi.string().custom(objectId).required(),
    customer: Joi.string().custom(objectId),
    name: Joi.string(),
    bookingStart: Joi.date()
      .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
      .utc()
      .greater("now")
      .required(),
    bookingEnd: Joi.date()
      .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
      .utc()
      .min(Joi.ref("bookingStart"))
      .required(),
    voucher: Joi.string().custom(objectId),
  }),
};

const getBookingById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

// const updateBookingById = {
//   params: Joi.object().keys({
//     id: Joi.string().custom(objectId),
//   }),
//   body: Joi.object()
//     .keys({
//       room: Joi.string().custom(objectId),
//       bookingStart: Joi.date()
//         .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
//         .utc()
//         .greater("now"),
//       bookingEnd: Joi.date()
//         .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
//         .utc()
//         .min(Joi.ref("bookingStart")),
//     })
//     .min(1),
// };

const cancelBookingById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const paymentBooking = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const getBookingsByHotelId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createBooking,
  getBookingById,
  paymentBooking,
  cancelBookingById,
  getBookingsByHotelId,
};
