const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");

const takeAttendance = {
  params: Joi.object().keys({ id: Joi.string().custom(objectId) }),
};

const queryManyAttendanceByHotelShiftId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

module.exports = { takeAttendance, queryManyAttendanceByHotelShiftId };
