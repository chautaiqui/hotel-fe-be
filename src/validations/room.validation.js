const Joi = require("joi").extend(require('@joi/date'));
const { objectId } = require("./custom/custom.validation");
const { roomStatusArr } = require("../customs/db/roomStatus");

const createRoom = {
    params: Joi.object().keys({
        id: Joi.string().custom(objectId),
    }),
    body: Joi.object().keys({
        name: Joi.string().required(),
        roomType: Joi.string().required(),
        status: Joi.string().valid(...roomStatusArr),
        facilities: Joi.array().items(Joi.object().keys({
            facility: Joi.string().required(),
            amount: Joi.number().integer().min(1).required(),
        })),
    }),
};

const getAllRoomByHotelId = {
    params: Joi.object().keys({
        id: Joi.string().custom(objectId),
    }),
};

const getRoomById = {
    params: Joi.object().keys({
        id: Joi.string().custom(objectId),
    }),
};

const updateRoomById = {
    params: Joi.object().keys({
        id: Joi.string().custom(objectId),
    }),
    body: Joi.object().keys({
        name: Joi.string(),
        roomType: Joi.string(),
        status: Joi.string().valid(...roomStatusArr),
        facilities: Joi.array().items(Joi.object().keys({
            facility: Joi.string().required(),
            amount: Joi.number().integer().min(1).required(),
        })),
    }).min(1),
};

module.exports = {
    createRoom,
    getAllRoomByHotelId,
    getRoomById,
    updateRoomById,
};