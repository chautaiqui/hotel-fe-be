const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");
const { voucherStatusArr } = require("../customs/db/voucherStatus");

// exports.simpleCronjob = {
//   body: Joi.object().keys({
//     time: Joi.date()
//       .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
//       .utc()
//       .greater("now"),
//     str: Joi.string(),
//   }),
// };

exports.createVoucher = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    roomType: Joi.string().required(),
    startDate: Joi.date()
      .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
      .utc()
      .greater("now")
      .required(),
    endDate: Joi.date()
      .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
      .utc()
      .min(Joi.ref("startDate"))
      .required(),
    discount: Joi.number().min(0).max(100).required(),
    description: Joi.string(),
    amount: Joi.number().integer().min(1).required(),
    status: Joi.string().valid(...voucherStatusArr),
  }),
};

exports.getVoucherById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

exports.getVouchersByHotelId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

exports.updateVoucherById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      roomType: Joi.string(),
      startDate: Joi.date()
        .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
        .utc(),
      endDate: Joi.date()
        .format(["DD-MM-YYYY HH:mm", "DD/MM/YYYY HH:mm"])
        .utc(),
      discount: Joi.number().min(0),
      description: Joi.string(),
      amount: Joi.number().integer().min(1),
      status: Joi.string().valid(...voucherStatusArr),
    })
    .min(1),
};

exports.customerGetVoucher = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    customer: Joi.string().custom(objectId).required(),
  }),
};
