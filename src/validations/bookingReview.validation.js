const Joi = require('joi').extend(require('@joi/date'));
const { objectId } = require('./custom/custom.validation');

const createReview = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		// customer: Joi.string().custom(objectId).required(),
		// hotel: Joi.string().custom(objectId).required(),
		rating: Joi.number().integer().min(0).max(5).required(),
		review: Joi.string().required(),
	}),
};

const findById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const findByHotelId = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const updateById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		rating: Joi.number().integer().min(0).max(5),
		review: Joi.string(),
	}),
};

module.exports = {
	createReview,
	findById,
	findByHotelId,
	updateById,
};
