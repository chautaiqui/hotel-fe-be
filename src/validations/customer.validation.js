const Joi = require("joi").extend(require("@joi/date"));
const { objectId, password, phone } = require("./custom/custom.validation");

const createCustomer = {
  body: Joi.object().keys({
    email: Joi.string().required().email(),
    password: Joi.string().required().custom(password),
    name: Joi.string().required(),
    birthday: Joi.date().format(["DD-MM-YYYY", "DD/MM/YYYY"]).utc(),
    phone: Joi.string().required().custom(phone),
    address: Joi.string().required(),
  }),
};

const login = {
  body: Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
  }),
};

const getCustomerById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const updateCustomerById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      email: Joi.string().email(),
      password: Joi.string().custom(password),
      name: Joi.string(),
      birthday: Joi.date().format(["DD-MM-YYYY", "DD/MM/YYYY"]).utc(),
      phone: Joi.string().custom(phone),
      address: Joi.string(),
    })
    .min(1),
};

const getVouchersByCustomerId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const getBookingsByCustomerId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

module.exports = {
  createCustomer,
  login,
  getCustomerById,
  updateCustomerById,
  getVouchersByCustomerId,
  getBookingsByCustomerId,
};
