const Joi = require('joi').extend(require('@joi/date'));
const { objectId } = require('./custom/custom.validation');

const rating = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		customer: Joi.string().custom(objectId).required(),
		rating: Joi.number().integer().min(0).max(5).required(),
		comment: Joi.string(),
	}),
};

const getRatings = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

module.exports = {
	rating,
	getRatings,
};
