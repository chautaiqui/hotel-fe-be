const Joi = require("joi").extend(require('@joi/date'));

const login = {
    body: Joi.object().keys({
        email: Joi.string().required(),
        password: Joi.string().required(),
    }),
}

module.exports = {
    login,
}