const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");

const createFacility = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    type: Joi.string().required(),
    name: Joi.string().required(),
    amount: Joi.number().integer().min(1).required(),
  }),
};

const getAllFacilityByHotelId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const getFacilityById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const updateFacilityById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object()
    .keys({
      name: Joi.string(),
      amount: Joi.number().integer().min(1),
    })
    .min(1),
};

module.exports = {
  createFacility,
  getAllFacilityByHotelId,
  getFacilityById,
  updateFacilityById,
};
