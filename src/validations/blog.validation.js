const Joi = require('joi').extend(require('@joi/date'));
const { objectId } = require('./custom/custom.validation');

const createBlog = {
	body: Joi.object().keys({
		title: Joi.string().required(),
		content: Joi.string().required(),
	}),
};

const getAllBlog = {
	query: Joi.object().keys({
		title: Joi.string(),
	}),
};

const getBlogById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

const updateBlogById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	body: Joi.object().keys({
		title: Joi.string(),
		content: Joi.string(),
	}),
};

const deleteBlogById = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
};

module.exports = {
	createBlog,
	getAllBlog,
	getBlogById,
	updateBlogById,
	deleteBlogById,
};
