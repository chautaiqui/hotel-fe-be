const Joi = require('joi').extend(require('@joi/date'));
const { objectId } = require('./custom/custom.validation');

const getHotelReport = {
	params: Joi.object().keys({
		id: Joi.string().custom(objectId),
	}),
	query: Joi.object().keys({
		month: Joi.number().integer().min(1).max(12).required(),
		year: Joi.number().integer().min(2021).required(),
	}),
};

const getAdminReport = {
	query: Joi.object().keys({
		month: Joi.number().integer().min(1).max(12).required(),
		year: Joi.number().integer().min(2021).required(),
	}),
};

module.exports = { getHotelReport, getAdminReport };
