const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");

const createRoomType = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    name: Joi.string().required(),
    capacity: Joi.number().integer().min(1).required(),
    price: Joi.number().integer().min(1000).required(),
    // facilities: Joi.array().items(Joi.object().keys({
    //     type: Joi.string().required(),
    //     amount: Joi.number().integer().min(1).required(),
    // })),
  }),
};

const getAllRoomTypeByHotelId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const getRoomTypeById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const updateRoomTypeById = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    name: Joi.string(),
    capacity: Joi.number().integer().min(1),
    price: Joi.number().integer().min(1000),
  }),
};

module.exports = {
  createRoomType,
  getAllRoomTypeByHotelId,
  getRoomTypeById,
  updateRoomTypeById,
};
