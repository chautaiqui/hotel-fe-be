const Joi = require("joi").extend(require("@joi/date"));
const { objectId } = require("./custom/custom.validation");

const createHotelShift = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
  body: Joi.object().keys({
    hotel: Joi.string().custom(objectId),
    timeInOut: Joi.string().required(),
    salaryCoefficient: Joi.number().integer().min(1).required(),
    date: Joi.number().required(),
    month: Joi.number().required(),
    year: Joi.number().required(),
  }),
};

const getAllHotelShiftByHotelId = {
  params: Joi.object().keys({
    id: Joi.string().custom(objectId),
  }),
};

const getHotelShiftById = {
  params: Joi.object().keys({ id: Joi.string().custom(objectId) }),
};

const addEmpToHotelShift = {
  params: Joi.object().keys({ id: Joi.string().custom(objectId) }),
};

const viewEmployeeInShift = {
  params: Joi.object().keys({ id: Joi.string().custom(objectId) }),
};

module.exports = {
  createHotelShift,
  getAllHotelShiftByHotelId,
  getHotelShiftById,
  addEmpToHotelShift,
  viewEmployeeInShift,
};
