const { Admin, Employee } = require("../models");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");

const login = async (email, password) => {
    let auth = await Admin.findOne({ email: email});
    let role = "admin";
    if (!auth) {
        auth = await Employee.findOne({ email: email });
        role = !auth ? role : auth.type;
    }
    if (!auth || !(await auth.isPasswordMatch(password))) {
        throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password.');
    }
    return {auth, role};
};

module.exports = {
    login,
}