const { Employee, Leave } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const Attendance = require('../models/attendance.model');
const { employeeReport } = require('./hotelReport1.service');
const mongoose = require('mongoose');

const createEmployee = async (employeeBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const employees = await Employee.create([employeeBody], opts);
		const employee = employees[0];

		await employeeReport(employee.hotel, employee._id);

		await session.commitTransaction();
		session.endSession();
		return employee;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const loginEmployeeWithEmailAndPassword = async (email, password) => {
	const employee = await Employee.findOne({ email: email });
	if (!employee || !(await employee.isPasswordMatch(password))) {
		throw new ApiError(
			httpStatus.UNAUTHORIZED,
			'Incorrect email or password.'
		);
	}
	return employee;
};

/**
 *
 * @param {Object} filter
 */
const getEmployeesByFilter = async (filter) => {
	const employees = await Employee.find(filter);
	if (!employees) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found employees with filter ${filter}.`
		);
	}
	return employees;
};

const getEmployeeById = async (employeeId) => {
	const employee = await Employee.findById(employeeId);
	if (!employee) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found employee with id ${employeeId}.`
		);
	}
	return employee;
};

const updateProfileById = async (employeeId, updateBody) => {
	const employee = await getEmployeeById(employeeId);
	if (updateBody.skills && !Array.isArray(updateBody.skills)) {
		updateBody.skills = [updateBody.skills];
	}
	Object.assign(employee, updateBody);
	await employee.save();
	return employee;
};

// const changePassword = async (employeeId, newPassword) => {
//     const employee = await updateProfileById(employeeId, { password: newPassword });
//     return employee;
// };

const getAllShiftByEmpId = async (empId) => {
	const shifts = await Employee.findById(empId).populate('shift');

	if (!shifts)
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found shift with id '${empId}'`
		);

	return shifts.shift;
};

const getAllAttendanceByEmpId = async (empId) => {
	const attendances = await Attendance.find({ employee: empId });

	if (!attendances)
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found attendance with id '${empId}'`
		);

	return attendances;
};

const getAttendanceByMonth = async (month, year, empId) => {
	const attendance = await Attendance.find({ employee: empId }).populate(
		'shifts'
	);
	if (!attendance)
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found shifts with id '${empId}'`
		);

	let currentAttendance = [];
	for (let i = 0; i < attendance.length; i++) {
		if (
			attendance[i].shifts.month == month &&
			attendance[i].shifts.year == year
		)
			currentAttendance.push(attendance[i]);
	}
	return currentAttendance;
};

const getSalaryByEmpId = async (month, year, empId) => {
	const shifts = await Attendance.find({ employee: empId }).populate(
		'shifts'
	);
	if (!shifts)
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found shifts with id '${empId}'`
		);

	let salary = 0;

	for (let i = 0; i < shifts.length; i++) {
		if (shifts[i].shifts.month == month && shifts[i].shifts.year == year)
			salary += shifts[i].shifts.salaryCoefficient;
	}

	return salary;
};

const getShiftBySpecificTime = async (month, year, empId) => {
	const shifts = await Employee.findById(empId).populate('shift');
	if (!shifts)
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found shift with id '${empId}'`
		);

	const shiftByTime = [];

	for (let i = 0; i < shifts.shift.length; i++) {
		if (shifts.shift[i].month == month && shifts.shift[i].year == year) {
			shiftByTime.push(shifts.shift[i]);
		}
	}

	return shiftByTime;
};

const applyForLeave = async (leaveBody) => {
	const leave = await Leave.create(leaveBody);
	return leave;
};

const getLeaveByEmpId = async (id) => {
	const leave = await Leave.find({ employee: id });
	return leave;
};

module.exports = {
	getAllShiftByEmpId,
	createEmployee,
	loginEmployeeWithEmailAndPassword,
	getEmployeesByFilter,
	getEmployeeById,
	updateProfileById,
	// changePassword,
	getAllAttendanceByEmpId,
	getSalaryByEmpId,
	getAttendanceByMonth,
	getShiftBySpecificTime,
	applyForLeave,
	getLeaveByEmpId,
};
