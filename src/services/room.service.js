const { Room, Hotel, RoomType } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const mongoose = require('mongoose');
const { roundUp } = require('../utils/round');

const createRoom = async (roomBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const rooms = await Room.create([roomBody], opts);
		const room = rooms[0];

		const roomType = await RoomType.findById(room.roomType);
		const hotel = await Hotel.findById(room.hotel);

		if (roomType.capacity > hotel.capacity) {
			hotel.capacity = roomType.capacity;
		}
		hotel.averagePrice.amount += 1;
		hotel.averagePrice.allValue += roomType.price;
		const avg = hotel.averagePrice.allValue / hotel.averagePrice.amount;
		hotel.averagePrice.avgValue = roundUp(avg);
		await hotel.save(opts);

		await session.commitTransaction();
		session.endSession();
		return room;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const getAllRoomByHotelId = async (hotelId) => {
	const rooms = await Room.find({ hotel: hotelId })
		.populate('roomType')
		.populate('bookings')
		.populate({
			path: 'facilities.facility',
			populate: { path: 'type', select: 'name -_id' },
		});
	return rooms;
};

const getRoomById = async (roomId) => {
	const room = await Room.findById(roomId)
		.populate('roomType')
		.populate('bookings')
		.populate({
			path: 'facilities.facility',
			populate: { path: 'type', select: 'name -_id' },
		});
	if (!room) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found room with id '${roomId}'`
		);
	}
	return room;
};

const updateRoomById = async (roomId, updateBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };
		const room = await getRoomById(roomId);

		if (updateBody.roomType) {
			const oldRoomType = await RoomType.findById(room.roomType);
			const newRoomType = await RoomType.findById(updateBody.roomType);
			const hotel = await Hotel.findById(room.hotel);

			if (newRoomType.capacity >= hotel.capacity) {
				hotel.capacity = newRoomType.capacity;
			} else {
				const capaRoomTypes = await RoomType.find({
					hotel: hotel._id,
					capacity: {
						$gt: newRoomType.capacity,
						$lte: hotel.capacity,
					},
				});

				let currentMaxCapacity = newRoomType.capacity;

				if (capaRoomTypes.length > 0) {
					for (const typeItem of capaRoomTypes) {
						const currentRoom = await Room.findOne({
							_id: { $ne: roomId },
							hotel: hotel._id,
							roomType: typeItem._id,
						});
						if (
							currentRoom &&
							currentMaxCapacity < typeItem.capacity
						) {
							currentMaxCapacity = typeItem.capacity;
						}
					}
				}

				hotel.capacity = currentMaxCapacity;
			}
			hotel.averagePrice.allValue =
				hotel.averagePrice.allValue +
				newRoomType.price -
				oldRoomType.price;
			const avg = hotel.averagePrice.allValue / hotel.averagePrice.amount;
			hotel.averagePrice.avgValue = roundUp(avg);
			await hotel.save(opts);
		}

		Object.assign(room, updateBody);
		await room.save(opts);

		await session.commitTransaction();
		session.endSession();
		return room;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

module.exports = {
	createRoom,
	getAllRoomByHotelId,
	getRoomById,
	updateRoomById,
};
