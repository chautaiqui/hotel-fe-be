const jwt = require("jsonwebtoken");
const authConfig = require("../config/auth.config");

const generateToken = async (id) => {
    let token = jwt.sign({ id: id }, authConfig.secret, {
        expiresIn: 86400, // 24 hours
    });

    return token;
};

module.exports = {
    generateToken,
}