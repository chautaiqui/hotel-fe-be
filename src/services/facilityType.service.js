const { FacilityType } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const createFacilityType = async (facilityTypeBody) => {
	const facilityType = await FacilityType.create(facilityTypeBody);
	return facilityType;
};

const getAllFacilityTypeByHotelId = async (hotelId) => {
	const facilityTypes = await FacilityType.find({ hotel: hotelId });
	return facilityTypes;
};

const getFacilityTypeById = async (facilityTypeId) => {
	const facilityType = await FacilityType.findById(facilityTypeId);
	if (!facilityType) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found facility type with id '${facilityTypeId}'`
		);
	}
	return facilityType;
};

/**
 *
 * @param {Object} filter
 * @returns
 */
const getFacilityTypeByNameandHotelId = async (name, hotelId) => {
	const facilityType = await FacilityType.findOne({
		name: name,
		hotel: hotelId,
	});
	if (!facilityType) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found facility type with name '${name}' of hotel id '${hotelId}'`
		);
	}
	return facilityType;
};

const updateFacilityTypeById = async (facilityTypeId, updateBody) => {
	const facilityType = await getFacilityTypeById(facilityTypeId);
	Object.assign(facilityType, updateBody);
	await facilityType.save();
	return facilityType;
};

module.exports = {
	createFacilityType,
	getAllFacilityTypeByHotelId,
	getFacilityTypeById,
	getFacilityTypeByNameandHotelId,
	updateFacilityTypeById,
};
