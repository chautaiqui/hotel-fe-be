const { Rating, Hotel, Customer } = require('../models');
const mongoose = require('mongoose');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { ratingReport } = require('./hotelReport1.service');

const createRating = async (hotelId, customerId, ratingValue, comment) => {
	const hotel = await Hotel.findById(hotelId);

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session }; // option for transaction

		const rating = await Rating.create(
			[
				{
					hotel: hotel._id,
					customer: customerId,
					rating: ratingValue,
					comment: comment
						? comment
						: "Customer's comment for hotel.",
				},
			],
			opts
		);

		hotel.rated.amount += 1;
		hotel.rated.allValue += ratingValue;
		hotel.rated.avgValue = hotel.rated.allValue / hotel.rated.amount;
		await hotel.save(opts);

		const nowTime = new Date(Date.now());
		const month = nowTime.getMonth();
		await ratingReport(hotel._id, 0, ratingValue, month + 1);

		await session.commitTransaction();
		session.endSession();
		return rating[0];
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const updateRating = async (hotelId, ratingId, ratingValue, comment) => {
	const hotel = await Hotel.findById(hotelId);
	const rating = await Rating.findById(ratingId);

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session }; // option for transaction

		const oldRating = rating.rating;
		const month = rating.updatedAt.getMonth() + 1;

		const value = ratingValue - rating.rating;
		Object.assign(rating, {
			rating: ratingValue,
			comment: comment ? comment : rating.comment,
		});
		await rating.save(opts);

		hotel.rated.allValue += value;
		hotel.rated.avgValue = hotel.rated.allValue / hotel.rated.amount;
		await hotel.save(opts);

		await ratingReport(hotel._id, oldRating, ratingValue, month + 1);

		await session.commitTransaction();
		session.endSession();
		return rating;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

exports.rating = async (hotelId, customerId, ratingValue, comment = '') => {
	const rating = await Rating.findOne({
		hotel: hotelId,
		customer: customerId,
	});
	if (!rating) {
		return await createRating(hotelId, customerId, ratingValue, comment);
	} else {
		return await updateRating(hotelId, rating._id, ratingValue, comment);
	}
};

exports.getRatings = async (hotelId) => {
	const ratings = await Rating.find({ hotel: hotelId }).populate('customer');
	return ratings;
};
