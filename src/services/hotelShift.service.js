const httpStatus = require("http-status");
const mongoose = require("mongoose");

const { HotelWorkingShift, Employee } = require("../models");
const ApiError = require("../utils/ApiError");

const createHotelShift = async (hotelShiftBody) => {
  const session = await mongoose.startSession();
  session.startTransaction();
  try {
    const opts = { session };
    const hotelShifts = await HotelWorkingShift.create([hotelShiftBody], opts);
    await session.commitTransaction();
    session.endSession();
    return hotelShifts;
  } catch (e) {
    await session.abortTransaction();
    session.endSession();
    throw new Error(e);
  }
};

const queryManyHotelShift = async (filter, options) => {
  const hotelShifts = await HotelWorkingShift.find(filter, options);

  if (!hotelShifts)
    throw new ApiError(
      httpStatus.NOT_FOUND,
      `Not found hotel shift with filter '${filter}'`
    );
  return hotelShifts;
};

const getHotelShiftById = async (hotelShiftId) => {
  const hotelShift = await HotelWorkingShift.findById(hotelShiftId);

  if (!hotelShift)
    throw new ApiError(
      httpStatus.NOT_FOUND,
      `Not found hotel shift with id: ${hotelShiftId}`
    );

  return hotelShift;
};

const addEmpToHotelShift = async (hotelShiftId, empId) => {
  const hotelShift = await HotelWorkingShift.findById(hotelShiftId);
  const emp = await Employee.findById(empId);

  if (!hotelShift)
    throw new ApiError(
      httpStatus.NOT_FOUND,
      `Not found hotel shift with id '${hotelShiftId}'`
    );
  if (!emp)
    throw new ApiError(
      httpStatus.NOT_FOUND,
      `Not found employee with id '${emp}'`
    );

  if (
    hotelShift.employeeList.includes(empId) === true ||
    emp.shift.includes(hotelShiftId) === true
  )
    throw new ApiError(
      httpStatus.BAD_REQUEST,
      `employee or hotel shift existed`
    );
  emp.shift.push(hotelShiftId);
  hotelShift.employeeList.push(empId);

  await emp.save();
  await hotelShift.save();

  return hotelShift;
};

const viewEmployeeInShift = async (hotelShiftId) => {
  const employees = await HotelWorkingShift.findById(hotelShiftId).populate(
    "employeeList"
  );

  if (!employees)
    throw new ApiError(
      httpStatus.NOT_FOUND,
      `Not found employee with id '${hotelShiftId}'`
    );

  return employees.employeeList;
};

module.exports = {
  createHotelShift,
  queryManyHotelShift,
  getHotelShiftById,
  addEmpToHotelShift,
  viewEmployeeInShift,
};
