const { Employee } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');

const createManager = async (managerBody) => {
	const manager = await Employee.create(managerBody);
	return manager;
};

const loginManagerWithEmailAndPassword = async (email, password) => {
	const manager = await Employee.findOne({ email: email });
	if (!manager || !(await manager.isPasswordMatch(password))) {
		throw new ApiError(
			httpStatus.UNAUTHORIZED,
			'Incorrect email or password.'
		);
	}
	return manager;
};

/**
 *
 * @param {Object} filter
 */
const getManagersByFilter = async (filter) => {
	const managers = await Employee.find(filter).populate('hotel');
	if (!managers) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found managers with filter ${filter}.`
		);
	}
	return managers;
};

const getManagerById = async (managerId) => {
	const manager = await (
		await Employee.findById(managerId)
	).populate('hotel');
	if (!manager) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found manager with id ${managerId}.`
		);
	}
	return manager;
};

const updateProfileById = async (managerId, updateBody) => {
	const manager = await getManagerById(managerId);
	if (updateBody.skills && !Array.isArray(updateBody.skills)) {
		updateBody.skills = [updateBody.skills];
	}
	Object.assign(manager, updateBody);
	await manager.save();
	return manager;
};

// const changePassword = async (managerId, newPassword) => {
//     const manager = await updateProfileById(managerId, { password: newPassword });
//     return manager;
// };

module.exports = {
	createManager,
	loginManagerWithEmailAndPassword,
	getManagersByFilter,
	getManagerById,
	updateProfileById,
	// changePassword,
};
