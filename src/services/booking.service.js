const {
	Booking,
	Room,
	RoomType,
	Hotel,
	Customer,
	Voucher,
} = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { voucherStatus } = require('../customs/db/voucherStatus');
const mongoose = require('mongoose');
const _ = require('lodash');
const {
	bookingStatus,
	bookingStatusArr,
} = require('../customs/db/bookingStatus');
const { roundUp } = require('../utils/round');

const createBooking = async (bookingBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const room = await Room.findById(bookingBody.room);
		room.bookings.push(booking[0]._id);
		await room.save(opts);

		const roomType = await RoomType.findById(room.roomType);
		const bookingDuration =
			(booking.bookingEnd - booking.bookingStart) / 86400000;
		let money = bookingDuration * roomType.price;

		bookingBody.totalMoney = money;
		const booking = await Booking.create([bookingBody], opts);

		await session.commitTransaction();
		session.endSession();
		return booking[0];
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const createBookingV2 = async (bookingBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const room = await Room.findById(bookingBody.room);
		room.bookings.push(booking[0]._id);
		await room.save(opts);

		const roomType = await RoomType.findById(room.roomType);
		const bookingDuration =
			(booking.bookingEnd - booking.bookingStart) / 86400000;
		let money = bookingDuration * roomType.price;

		if (bookingBody.voucher) {
			const customer = await Customer.findById(bookingBody.customer);

			const voucher = await Voucher.findById(bookingBody.voucher);
			if (!voucher) {
				throw new ApiError(
					httpStatus.NOT_FOUND,
					`Not found voucher with id '${voucherId}'`
				);
			}
			if (voucher.status === voucherStatus.UNAVAILABLE) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is now unavailable.`
				);
			}
			if (!customer.voucher.includes(voucher._id)) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Customer ${customer.name} cannot use voucher '${voucher._id}'`
				);
			}
			if (voucher.roomType.toString() !== roomType._id.toString()) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is unavailable for roomType '${roomType.name}'.`
				);
			}
			if (
				booking.bookingEnd < voucher.startDate ||
				booking.bookingEnd > voucher.endDate
			) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is unavailable'.`
				);
			}

			money = (money * (100 - voucher.discount)) / 100;

			voucher.usingCustomers.push(customer._id);
			await voucher.save(opts);

			const voucherArr = customer.voucher.filter(
				(item) => item.toString() != bookingBody.voucher.toString()
			);
			customer.voucher = voucherArr;
			await customer.save(opts);
		}

		bookingBody.totalMoney = roundUp(money);
		const booking = await Booking.create([bookingBody], opts);

		await session.commitTransaction();
		session.endSession();
		return booking[0];
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

/**
 *
 * @param {Object} filter
 * @param {Object} options
 * @returns
 */
const queryManyBooking = async (filter, options) => {
	const bookings = await Booking.find(filter, options);
	if (!bookings) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found bookings with filter '${filter}'`
		);
	}
	return bookings;
};

const getBookingById = async (bookingId) => {
	const booking = await Booking.findById(bookingId).populate('room customer');
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id '${bookingId}'`
		);
	}
	await booking.room.populate('roomType').execPopulate();
	return booking;
};

const updateBookingById = async (bookingId, updateBody) => {
	const booking = await Booking.findById(bookingId);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id '${bookingId}'`
		);
	}

	if (!updateBody.room) {
		Object.assign(booking, updateBody);
		await booking.save();
		return booking;
	}

	// updateBody.room
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const oldRoom = await Room.findById(booking.room);
		const arr = oldRoom.bookings.filter((item) => item != bookingId);
		oldRoom.bookings = arr;
		await oldRoom.save(opts);

		const newRoom = await Room.findById(updateBody.room);
		if (!newRoom) {
			throw new ApiError(
				httpStatus.NOT_FOUND,
				`Not found room with id '${updateBody.room}'`
			);
		}
		newRoom.bookings.push(booking._id);
		await newRoom.save(opts);

		Object.assign(booking, updateBody);
		await booking.save(opts);

		await session.commitTransaction();
		session.endSession();
		return booking;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const cancelBookingById = async (bookingId) => {
	const booking = await Booking.findById(bookingId);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id '${bookingId}'`
		);
	}

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const room = await Room.findById(booking.room);
		const arr = room.bookings.filter((item) => item != bookingId);
		room.bookings = arr;
		await room.save(opts);

		// await booking.remove(opts);
		booking.bookingStatus = bookingStatus.CANCELLED;
		await booking.save(opts);

		await session.commitTransaction();
		session.endSession();
		return true;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const paymentBooking = async (bookingBody) => {
	const booking = await Booking.findById(bookingBody.id);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id '${bookingBody.id}'`
		);
	}

	const room = await Room.findById(booking.room);
	const roomType = await RoomType.findById(room.roomType);
	const customer = await Customer.findById(booking.customer);

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const bookingDuration =
			(booking.bookingEnd - booking.bookingStart) / 86400000;
		let money = bookingDuration * roomType.price;

		if (bookingBody.voucher) {
			const voucher = await Voucher.findById(bookingBody.voucher);
			if (!voucher) {
				throw new ApiError(
					httpStatus.NOT_FOUND,
					`Not found voucher with id '${voucherId}'`
				);
			}
			if (voucher.status === voucherStatus.UNAVAILABLE) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is now unavailable.`
				);
			}
			if (!customer.voucher.includes(voucher._id)) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Customer ${customer.name} cannot use voucher '${voucher._id}'`
				);
			}
			if (voucher.roomType.toString() !== roomType._id.toString()) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is unavailable for roomType '${roomType.name}'.`
				);
			}
			if (
				booking.bookingEnd < voucher.startDate ||
				booking.bookingEnd > voucher.endDate
			) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is unavailable'.`
				);
			}

			money = (money * (100 - voucher.discount)) / 100;

			voucher.usingCustomers.push(customer._id);
			await voucher.save(opts);

			const voucherArr = customer.voucher.filter(
				(item) => item.toString() != bookingBody.voucher.toString()
			);
			customer.voucher = voucherArr;
			await customer.save(opts);
		}

		booking.totalMoney = money;

		await booking.save(opts);
		booking.bookingStatus = bookingStatus.COMPLETED;
		const bookingArr = room.bookings.filter(
			(item) => item.toString() !== bookingBody.id.toString()
		);
		room.bookings = bookingArr;
		await room.save(opts);

		await session.commitTransaction();
		session.endSession();
		return booking;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const paymentBookingV2 = async (bookingId) => {
	const booking = await Booking.findById(bookingId);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id '${bookingId}'`
		);
	}

	booking.bookingStatus = bookingStatus.COMPLETED;
	await booking.save();
	return booking;
};

module.exports = {
	createBooking,
	queryManyBooking,
	getBookingById,
	updateBookingById,
	cancelBookingById,
	paymentBooking,
	createBookingV2,
	paymentBookingV2,
};
