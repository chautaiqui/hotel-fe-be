const { Hotel, Employee, Leave } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { employeeType } = require('../customs/db/employeeType');
const mongoose = require('mongoose');
const HotelWorkingShift = require('../models/hotelShift.model');
const { getPagination } = require('../utils/getPagination');

const createHotel = async (hotelBody) => {
	const hotel = await Hotel.create(hotelBody);
	return hotel;
};

const getAllHotel = async () => {
	const hotel = await Hotel.find({}).populate('manager');
	return hotel;
};

const getHotels = async (filter, options) => {
	const {
		province,
		capacity,
		averagePrice,
		maxPrice,
		minPrice,
		page,
		pageSize,
	} = filter;

	const query = {};
	if (province) {
		query.$text = { $search: `${province}` };
	}

	if (capacity) {
		query.capacity = { $gte: capacity };
	}

	if (maxPrice) {
		query['averagePrice.avgValue'] = {
			$gte: minPrice ? minPrice : 0,
			$lte: maxPrice,
		};
	} else {
		query['averagePrice.avgValue'] = {
			$gte: minPrice ? minPrice : 0,
		};
	}

	const { limit, offset } = getPagination(page, pageSize);

	if (averagePrice == 'asc') {
		// const hotels = await Hotel.find(query).sort('averagePrice.avgValue');
		const hotels = await Hotel.paginate(query, {
			limit,
			offset,
			sort: 'averagePrice.avgValue',
		});
		return {
			totalItems: hotels.totalDocs,
			hotels: hotels.docs,
			totalPages: hotels.totalPages,
			currentPage: hotels.page - 1,
		};
	} else if (averagePrice == 'desc') {
		const hotels = await Hotel.paginate(query, {
			limit,
			offset,
			sort: '-averagePrice.avgValue',
		});
		return {
			totalItems: hotels.totalDocs,
			hotels: hotels.docs,
			totalPages: hotels.totalPages,
			currentPage: hotels.page - 1,
		};
	} else {
		const hotels = await Hotel.paginate(query, {
			limit,
			offset,
		});
		return {
			totalItems: hotels.totalDocs,
			hotels: hotels.docs,
			totalPages: hotels.totalPages,
			currentPage: hotels.page - 1,
		};
	}
};

const getHotelById = async (hotelId) => {
	const hotel = await Hotel.findById(hotelId).populate('manager');
	if (!hotel) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found hotel with id ${hotelId}.`
		);
	}
	return hotel;
};

const updateHotelById = async (hotelId, updateBody) => {
	const hotel = await getHotelById(hotelId);
	Object.assign(hotel, updateBody);
	if (updateBody.files) {
		// hotel.imgs = [...hotel.imgs, ...updateBody.files];
		hotel.imgs = [...updateBody.files];
	}
	await hotel.save();
	return hotel;
};

const changeManager = async (hotelId, managerId) => {
	const hotel = await Hotel.findById(hotelId);
	if (!hotel) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found hotel with id ${hotelId}.`
		);
	}

	const manager = await Employee.findById(managerId);
	if (!manager) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found manager with id ${managerId}.`
		);
	}
	if (manager.type != employeeType.MANAGER) {
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`This employee is not a manager.`
		);
	}

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session }; // option for transaction

		hotel.manager = manager._id;
		await hotel.save(opts);

		if (manager.hotel) {
			const oldHotel = await Hotel.findById(manager.hotel);
			if (!oldHotel) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Error when changing manager.`
				);
			}

			oldHotel.manager = null;
			await oldHotel.save(opts);
		}

		manager.hotel = hotel._id;
		await manager.save(opts);

		await session.commitTransaction();
		session.endSession();
		return hotel;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const getHotelsWithoutManager = async (filter) => {
	// const hotels = await Hotel.find({ manager: null });
	// return hotels;
	const { page, pageSize } = filter;
	const { limit, offset } = getPagination(page, pageSize);
	const hotels = await Hotel.paginate(
		{ manager: null },
		{
			limit,
			offset,
		}
	);
	return {
		totalItems: hotels.totalDocs,
		hotels: hotels.docs,
		totalPages: hotels.totalPages,
		currentPage: hotels.page - 1,
	};
};

const confirmLeave = async (id) => {
	//leave id
	const leave = await Leave.findById(id).populate('hotelShift');
	const employeeById = await Leave.findById(id).populate('employee');
	if (!leave)
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`Not found this leave with id ${id}.`
		);
	const employeeShift = await Employee.findById(employeeById.employee._id);
	const shift = await HotelWorkingShift.findById(leave.hotelShift._id);
	const employee = leave.employee;
	//Dayoff - 1 day when confirm leave
	let dayoff = await Employee.findById(employeeById.employee._id);
	dayoff.remainingDayoffNumber = dayoff.remainingDayoffNumber - 1;
	if (dayoff.remainingDayoffNumber === 0)
		throw new ApiError(httpStatus.BAD_REQUEST, 'Not enough remain dayoff');
	dayoff.save();
	//remove employee in shift
	shift.employeeList.pull(employee);
	//remove shift in employee
	employeeShift.shift.pull(leave.hotelShift._id);
	employeeShift.save();
	shift.save();
	return shift.employeeList;
};

const getLeaveByHotelId = async (id) => {
	const leave = await Leave.find({ hotel: id });
	if (!leave)
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`Not found this leave with id ${id}.`
		);

	return leave;
};

const getLeaveBySpecificTime = async (id, month, year) => {
	const leave = await getLeaveByHotelId(id);
	if (!leave)
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found leave with id '${id}'`
		);
	let leaveByTime = [];
	for (let i = 0; i < leave.length; i++) {
		if (leave[i].month === month && leave[i].year === year)
			leaveByTime.push(leave[i]);
	}
	return leaveByTime;
};

module.exports = {
	createHotel,
	getAllHotel,
	getHotelById,
	updateHotelById,
	changeManager,
	confirmLeave,
	getLeaveByHotelId,
	getLeaveBySpecificTime,
	getHotelsWithoutManager,
	getHotels,
};
