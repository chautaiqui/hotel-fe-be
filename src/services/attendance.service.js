const httpStatus = require("http-status");
const mongoose = require("mongoose");

const { HotelWorkingShift, Employee, Attendance } = require("../models");
const ApiError = require("../utils/ApiError");

const takeAttendance = async (bodyAttendance) => {
  const session = await mongoose.startSession();
  session.startTransaction();

  try {
    const opts = { session };

    const attendances = await Attendance.create([bodyAttendance], opts);

    await session.commitTransaction();
    session.endSession();
    return { attendances };
    //return data
  } catch (e) {
    await session.abortTransaction();
    session.endSession();
    throw new Error(e);
  }
};

const queryManyAttendanceByHotelShiftId = async (filter, options) => {
  const attendances = await Attendance.find(filter, options);

  if (!attendances)
    throw new ApiError(
      httpStatus.NOT_FOUND,
      `Not found attendance with filter '${filter}'`
    );
  return attendances;
};

module.exports = {
  takeAttendance,
  queryManyAttendanceByHotelShiftId,
};
