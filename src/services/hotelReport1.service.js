const httpStatus = require('http-status');
const { HotelReport, Hotel } = require('../models');
const ApiError = require('../utils/ApiError');
const mongoose = require('mongoose');

const getReport = async (hotelId) => {
	const nowTime = new Date(Date.now());
	const month = nowTime.getMonth() + 1;
	const year = nowTime.getFullYear();
	const hotelReport = await HotelReport.findOne({
		hotel: hotelId,
		month,
		year,
	});
	if (hotelReport) {
		return hotelReport;
	} else {
		const hotel = await Hotel.findById(hotelId);
		const newHotelReport = await HotelReport.create({
			hotel: hotel._id,
			manager: hotel.manager,
			month,
			year,
		});
		return newHotelReport;
	}
};

/**
 * report for booking and roomType
 * booking: Booking model
 */
const bookingReport = async (hotelId, booking, roomType) => {
	const hotelReport = await getReport(hotelId);

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		hotelReport.bookingReport.amount += 1;
		hotelReport.bookingReport.totalMoney += booking.totalMoney;

		let roomTypeReport = hotelReport.roomTypeReport.find(
			(item) => item.roomType.toString() === roomType.toString()
		);

		if (roomTypeReport) {
			roomTypeReport.bookingAmount += 1;
			roomTypeReport.totalMoney += booking.totalMoney;
		} else {
			roomTypeReport = {
				roomType,
				bookingAmount: 0,
				totalMoney: 0,
			};
			roomTypeReport.bookingAmount += 1;
			roomTypeReport.totalMoney += booking.totalMoney;
			hotelReport.roomTypeReport.push(roomTypeReport);
		}

		await hotelReport.save(opts);
		await session.commitTransaction();
		session.endSession();
		return booking;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const facilityReport = async (hotelId, facilityId, amount) => {
	const hotelReport = await getReport(hotelId);
	const action = amount >= 0 ? 'add' : 'delete';

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const facilityReport = {
			facility: facilityId,
			amount: Math.abs(amount),
			action,
			time: Date.now(),
		};
		hotelReport.facilityReport.push(facilityReport);
		await hotelReport.save(opts);

		await session.commitTransaction();
		session.endSession();
		return facilityId;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const employeeReport = async (hotelId, employeeId, action = 'add') => {
	const hotelReport = await getReport(hotelId);

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const employeeReport = {
			employee: employeeId,
			action,
			time: Date.now(),
		};
		hotelReport.employeeReport.push(employeeReport);
		await hotelReport.save(opts);

		await session.commitTransaction();
		session.endSession();
		return employeeId;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const ratingReport = async (hotelId, oldRating, newRating, month) => {
	const hotelReport = await getReport(hotelId);

	let oldValue = 'six';
	switch (oldRating) {
		case 1:
			oldValue = 'one';
			break;
		case 2:
			oldValue = 'two';
			break;
		case 3:
			oldValue = 'three';
			break;
		case 4:
			oldValue = 'four';
			break;
		case 5:
			oldValue = 'five';
			break;
		default:
			oldValue = 'six';
			break;
	}

	let newValue = 'zero';
	switch (newRating) {
		case 1:
			newValue = 'one';
			break;
		case 2:
			newValue = 'two';
			break;
		case 3:
			newValue = 'three';
			break;
		case 4:
			newValue = 'four';
			break;
		case 5:
			newValue = 'five';
			break;
		default:
			newValue = 'one';
			break;
	}

	if (month == hotelReport.month) {
		if (oldValue == 'six') {
			hotelReport.ratingReport[`${newValue}`] += 1;
			hotelReport.ratingReport.allRating += 1;
		} else {
			hotelReport.ratingReport[`${oldValue}`] -= 1;
			hotelReport.ratingReport[`${newValue}`] += 1;
		}
	} else {
		hotelReport.ratingReport[`${oldValue}`] -= 1;
		hotelReport.ratingReport[`${newValue}`] += 1;
	}
	const sumRating =
		hotelReport.ratingReport.one * 1 +
		hotelReport.ratingReport.two * 2 +
		hotelReport.ratingReport.three * 3 +
		hotelReport.ratingReport.four * 4 +
		hotelReport.ratingReport.five * 5;
	hotelReport.ratingReport.avg =
		sumRating / hotelReport.ratingReport.allRating;

	await hotelReport.save();
	return true;
};

const bookingRatingReport = async (hotelId, oldRating, newRating, month) => {
	const hotelReport = await getReport(hotelId);

	if (!hotelReport.bookingRatingReport) {
		hotelReport.bookingRatingReport = {
			one: 0,
			two: 0,
			three: 0,
			four: 0,
			five: 0,
			allRating: 0,
			avg: 0,
		};
		await hotelReport.save();
	}

	let oldValue = 'six';
	switch (oldRating) {
		case 1:
			oldValue = 'one';
			break;
		case 2:
			oldValue = 'two';
			break;
		case 3:
			oldValue = 'three';
			break;
		case 4:
			oldValue = 'four';
			break;
		case 5:
			oldValue = 'five';
			break;
		default:
			oldValue = 'six';
			break;
	}

	let newValue = 'zero';
	switch (newRating) {
		case 1:
			newValue = 'one';
			break;
		case 2:
			newValue = 'two';
			break;
		case 3:
			newValue = 'three';
			break;
		case 4:
			newValue = 'four';
			break;
		case 5:
			newValue = 'five';
			break;
		default:
			newValue = 'one';
			break;
	}

	if (month == hotelReport.month) {
		if (oldValue == 'six') {
			hotelReport.bookingRatingReport[`${newValue}`] += 1;
			hotelReport.bookingRatingReport.allRating += 1;
		} else {
			hotelReport.bookingRatingReport[`${oldValue}`] -= 1;
			hotelReport.bookingRatingReport[`${newValue}`] += 1;
		}
	} else {
		hotelReport.bookingRatingReport[`${oldValue}`] -= 1;
		hotelReport.bookingRatingReport[`${newValue}`] += 1;
	}
	const sumRating =
		hotelReport.bookingRatingReport.one * 1 +
		hotelReport.bookingRatingReport.two * 2 +
		hotelReport.bookingRatingReport.three * 3 +
		hotelReport.bookingRatingReport.four * 4 +
		hotelReport.bookingRatingReport.five * 5;
	hotelReport.bookingRatingReport.avg =
		sumRating / hotelReport.bookingRatingReport.allRating;

	await hotelReport.save();
	return true;
};

module.exports = {
	getReport,
	bookingReport,
	facilityReport,
	employeeReport,
	ratingReport,
	bookingRatingReport,
};
