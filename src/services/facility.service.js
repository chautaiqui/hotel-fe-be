const { Facility, FacilityType } = require('../models');
const httpStatus = require('http-status');
const mongoose = require('mongoose');
const ApiError = require('../utils/ApiError');
const { facilityReport } = require('./hotelReport1.service');

const createFacility = async (facilityBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session }; // option for transaction
		const facilities = await Facility.create([facilityBody], opts);
		const facility = facilities[0];

		const facilityType = await FacilityType.findById(facility.type);
		facilityType.amount += facility.amount;
		facilityType.availableAmount += facility.availableAmount;
		await facilityType.save(opts);

		await facilityReport(facility.hotel, facility._id, facility.amount);

		await session.commitTransaction();
		session.endSession();
		return facility;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

/**
 *
 * @param {Object} filter
 * @param {Object} options
 * @returns
 */
const queryManyFacility = async (filter, options) => {
	const facilities = await Facility.find(filter, options).populate({
		path: 'type',
		select: 'name -_id',
	});
	if (!facilities) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found facility with filter '${filter}'`
		);
	}
	return facilities;
};

const getAllFacilityByHotelId = async (hotelId) => {
	const facilitys = await Facility.find({ hotel: hotelId }).populate('type');
	return facilitys;
};

const getFacilityById = async (facilityId) => {
	const facility = await Facility.findById(facilityId).populate('type');
	if (!facility) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found facility with id '${facilityId}'`
		);
	}
	return facility;
};

const getFacilityByNameAndHotelId = async (name, hotelId) => {
	const facility = await Facility.findOne({ name: name, hotel: hotelId });
	if (!facility) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found facility '${name}' of hotel id '${hotelId}'`
		);
	}
	return facility;
};

const updateFacilityById = async (facilityId, updateBody) => {
	const facility = await getFacilityById(facilityId);

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session }; // option for transaction
		let newAmount = 0;

		//update amount of facility type
		if (updateBody.amount) {
			newAmount = updateBody.amount - facility.amount;
			const facilityType = await FacilityType.findById(facility.type);
			facilityType.amount += newAmount;
			facilityType.availableAmount += newAmount;
			await facilityType.save(opts);

			updateBody.availableAmount = facility.availableAmount + newAmount;

			await facilityReport(facility.hotel, facility._id, newAmount);
		}

		Object.assign(facility, updateBody);
		await facility.save(opts);

		// update return body
		facility.type.amount += newAmount;
		facility.type.availableAmount += newAmount;

		await session.commitTransaction();
		session.endSession();
		return facility;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

/**
 *
 * @param {[{String, Number}]} facilities
 */
const getFacilitiesForRoom = async (facilities, hotelId) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };
		const facilityArr = [];
		for (const item of facilities) {
			// update available amount of facility and facilityType
			const facility = await getFacilityByNameAndHotelId(
				item.facility,
				hotelId
			);
			facility.availableAmount -= Number(item.amount);
			if (facility.availableAmount < 0) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Not enough vailable amount of '${facility.name}' facility.`
				);
			}
			await facility.save(opts);

			const facilityType = await FacilityType.findById(facility.type);
			facilityType.availableAmount -= Number(item.amount);
			await facilityType.save(opts);
			//

			facilityArr.push({
				facility: facility._id,
				amount: item.amount,
			});
		}

		await session.commitTransaction();
		session.endSession();
		return facilityArr;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const removeFacilitiesForRoom = async (facilities, hotelId) => {
	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };
		for (const item of facilities) {
			const facility = await getFacilityById(item.facility);
			facility.availableAmount += Number(item.amount);
			await facility.save(opts);

			const facilityType = await FacilityType.findById(facility.type);
			facilityType.availableAmount += Number(item.amount);
			await facilityType.save(opts);
		}

		await session.commitTransaction();
		session.endSession();
		return true;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

module.exports = {
	queryManyFacility,
	createFacility,
	getAllFacilityByHotelId,
	getFacilityById,
	getFacilityByNameAndHotelId,
	updateFacilityById,
	getFacilitiesForRoom,
	removeFacilitiesForRoom,
};
