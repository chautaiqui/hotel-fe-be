const { Voucher, Customer } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const mongoose = require('mongoose');
const { voucherStatus } = require('../customs/db/voucherStatus');
// const CronJob = require('cron').CronJob;

const createVoucher = async (voucherBody) => {
	const voucher = await Voucher.create(voucherBody);

	// create cron-job
	// const time = voucher.endDate;
	// const date = new Date(
	// 	time.getUTCFullYear(),
	// 	time.getUTCMonth(),
	// 	time.getUTCDate(),
	// 	time.getUTCHours(),
	// 	time.getUTCMinutes(),
	// 	0
	// );
	// const job = new CronJob(
	// 	date,
	// 	async function (VoucherModel, voucherId, status) {
	// 		const voucher = await VoucherModel.findById(voucherId);
	// 		voucher.status = status;
	// 		await voucher.save();
	// 	}.bind(null, Voucher, voucher._id, voucherStatus.UNAVAILABLE),
	// 	null,
	// 	true,
	// 	0
	// );
	// job.start();
	return voucher;
};

const getAllVoucher = async () => {
	const vouchers = await Voucher.find({}).populate('roomType hotel');
	return vouchers;
};

const getAvailableVoucher = async () => {
	const vouchers = await Voucher.find({
		status: voucherStatus.AVAILABLE,
	}).populate('roomType hotel');
	return vouchers;
};

const getVouchersByHotelId = async (hotelId) => {
	const vouchers = await Voucher.find({ hotel: hotelId }).populate(
		'roomType'
	);
	return vouchers;
};

const getVoucherById = async (voucherId) => {
	const voucher = await Voucher.findById(voucherId).populate(
		'roomType hotel'
	);
	if (!voucher) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found voucher with id '${voucherId}'`
		);
	}
	return voucher;
};

const updateVoucherById = async (voucherId, updateBody) => {
	const voucher = await getVoucherById(voucherId);
	if (updateBody.amount) {
		updateBody.remainingAmount =
			voucher.remainingAmount + (updateBody.amount - voucher.amount);
		if (updateBody.remainingAmount < 0) {
			throw new ApiError(
				httpStatus.BAD_REQUEST,
				'Updated mount is invalid, it causes remainingAmount lower than 0.'
			);
		}
	}
	Object.assign(voucher, updateBody);
	await voucher.save();
	return voucher;
};

const customerGetVoucher = async (voucherId, customerId) => {
	const customer = await Customer.findById(customerId);
	if (!customer) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found customer with id '${customerId}'`
		);
	}

	const voucher = await Voucher.findById(voucherId);
	if (!voucher) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found voucher with id '${voucherId}'`
		);
	}
	if (
		voucher.remainingAmount == 0 ||
		voucher.status == voucherStatus.UNAVAILABLE
	) {
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`This voucher is unavailable to get.`
		);
	}

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		if (
			customer.voucher.includes(voucher._id) ||
			voucher.customers.includes(customer._id)
		) {
			throw new ApiError(
				httpStatus.BAD_REQUEST,
				`Customer ${customer.name} has already taken this voucher.`
			);
		}

		customer.voucher.push(voucher._id);
		await customer.save(opts);

		voucher.customers.push(customer._id);
		voucher.remainingAmount -= 1;
		await voucher.save(opts);

		await session.commitTransaction();
		session.endSession();
		return voucher;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

module.exports = {
	createVoucher,
	getAllVoucher,
	getAvailableVoucher,
	getVouchersByHotelId,
	getVoucherById,
	updateVoucherById,
	customerGetVoucher,
};
