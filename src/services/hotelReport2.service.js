const httpStatus = require('http-status');
const { HotelReport } = require('../models');
const ApiError = require('../utils/ApiError');
const { Hotel } = require('../models');

const getReport = async (hotelId, month, year) => {
	const hotelReport = await HotelReport.findOne({
		hotel: hotelId,
		month,
		year,
	})
		.populate('hotel', 'name')
		.populate('manager', 'name')
		.populate('roomTypeReport.roomType', 'name')
		.populate('facilityReport.facility', 'name')
		.populate('employeeReport.employee', 'name');

	if (hotelReport) {
		return hotelReport;
	} else {
		const nowTime = new Date(Date.now());
		const nowMonth = nowTime.getMonth() + 1;
		const nowYear = nowTime.getFullYear();

		if (month != nowMonth || year != nowYear) {
			throw new ApiError(
				httpStatus.NOT_FOUND,
				`Not found report for ${month}/${year}`
			);
		}
		const hotel = await Hotel.findById(hotelId);
		const newHotelReport = await HotelReport.create({
			hotel: hotel._id,
			manager: hotel.manager,
			month,
			year,
		});
		return newHotelReport;
	}
};

const getAdminReport = async (month, year) => {
	let reports = await HotelReport.find({
		month,
		year,
	})
		.populate('hotel', 'name')
		.populate('manager', 'name');

	const nowTime = new Date(Date.now());
	const nowMonth = nowTime.getMonth() + 1;
	const nowYear = nowTime.getFullYear();
	if (month == nowMonth && year == nowYear) {
		const ids = reports.map((report) => report.hotel._id);
		const hotels = await Hotel.find({ _id: { $nin: ids } }).populate({
			path: 'manager',
			select: 'name',
		});

		const hotelReports = await Promise.all(
			hotels.map(async (hotel) => {
				const newHotelReport = await HotelReport.create({
					hotel: hotel._id,
					manager: hotel.manager,
					month,
					year,
				});
				const report = await HotelReport.findById(newHotelReport._id)
					.populate('hotel', 'name')
					.populate('manager', 'name');

				if (!report.manager) {
					report.managerc = 'Not have manager yet.';
				}

				return report;
			})
		);

		reports = [...reports, ...hotelReports];
	}

	reports = reports.map((report) => {
		report.roomTypeReport = undefined;
		report.facilityReport = undefined;
		report.employeeReport = undefined;

		return report;
	});

	return reports;
};

module.exports = {
	getReport,
	getAdminReport,
};
