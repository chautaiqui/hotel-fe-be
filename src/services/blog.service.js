const { Blog } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { documentStatus } = require('../customs/db/documentStatus');

const createBlog = async (blogBody) => {
	const blog = await Blog.create(blogBody);
	return blog;
};

const getAllBlog = async (title = undefined) => {
	let filter = { docStatus: documentStatus.AVAILABLE };

	if (title) {
		filter.title = {
			$regex: `.*${title}.*`,
			$options: 'i',
		};
	}

	const blogs = await Blog.find(filter);
	return blogs;
};

const getBlogById = async (blogId) => {
	const blog = await Blog.findById(blogId);
	if (!blog) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found blog with id '${blogId}'`
		);
	}
	return blog;
};

const updateBlogById = async (blogId, updateBody) => {
	const blog = await getBlogById(blogId);
	Object.assign(blog, updateBody);
	await blog.save();
	return blog;
};

const deleteBlogById = async (blogId) => {
	const blog = await updateBlogById(blogId, {
		docStatus: documentStatus.UNAVAILABLE,
	});
	return blog;
};

module.exports = {
	createBlog,
	getAllBlog,
	getBlogById,
	updateBlogById,
	deleteBlogById,
};
