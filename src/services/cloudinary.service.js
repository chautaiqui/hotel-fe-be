const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const cloudinary = require('cloudinary').v2;
const streamifier = require('streamifier');

const cloudinaryConfig = require("../config/cloudinary.config");
cloudinary.config(cloudinaryConfig);

const streamUpload = (file) => {
  return new Promise((resolve, reject) => {
    let stream = cloudinary.uploader.upload_stream(
      (error, result) => {
        if (result) {
          resolve(result.url);
        } else {
          reject(error);
        }
      }
    );
    streamifier.createReadStream(file.buffer).pipe(stream);
  });
}

/**
 * 
 * @param {*} req 
 * @returns {String} // url of image 
 */
const uploadImg = async (img) => {
    if (!img) {
      throw new ApiError(httpStatus.BAD_REQUEST, "File upload is empty.");
    }

    let result = await streamUpload(img);
    return result;
};

/**
 * upload array of img
 */
const uploadManyImg = async (imgs) => {
  if (!imgs) {
    throw new ApiError(httpStatus.BAD_REQUEST, "File upload is empty.");
  }

  const urls = [];
  for (const img of imgs) {
    const result = await streamUpload(img);
    urls.push(result);
  }

  return urls;
};

module.exports = {
  uploadImg,
  uploadManyImg
};