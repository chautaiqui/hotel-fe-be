const { Booking, BookingReview, Hotel, Customer } = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const mongoose = require('mongoose');
const { bookingRatingReport } = require('./hotelReport1.service');

const createReview = async (bookingId, reviewBody) => {
	const booking = await Booking.findById(bookingId);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id ${id}`
		);
	}

	const oldReview = await BookingReview.findOne({ booking: bookingId });
	if (oldReview) {
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`Booking with id ${bookingId} is already reviewed.`
		);
	}

	if (booking.isPaid == false) {
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`Booking with id ${bookingId} need to be paid before being reviewed.`
		);
	}

	reviewBody.booking = booking._id;
	reviewBody.customer = booking.customer;
	reviewBody.hotel = booking.hotel;

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const reviews = await BookingReview.create([reviewBody], opts);
		const review = reviews[0];

		// update hotel's bookingRating
		const hotel = await Hotel.findById(review.hotel);
		if (!hotel.bookingRating) {
			hotel.bookingRating = {
				avgValue: 0,
				allValue: 0,
				amount: 0,
			};
			await hotel.save(opts);
		}
		hotel.bookingRating.amount += 1;
		hotel.bookingRating.allValue += review.rating;
		hotel.bookingRating.avgValue =
			hotel.bookingRating.allValue / hotel.bookingRating.amount;
		await hotel.save(opts);

		// update hotel report
		const nowTime = new Date(Date.now());
		const month = nowTime.getMonth();
		await bookingRatingReport(hotel._id, 0, review.rating, month + 1);

		await session.commitTransaction();
		session.endSession();
		return review;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const findById = async (reviewId) => {
	const review = await BookingReview.findById(reviewId).populate(
		'booking hotel customer'
	);
	if (!review) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found review with id ${reviewId}`
		);
	}

	return review;
};

const findByHotelId = async (hotelId) => {
	const reviews = await BookingReview.find({ hotel: hotelId }).populate(
		'booking hotel customer'
	);

	return reviews;
};

const updateById = async (reviewId, updateBody) => {
	const review = await BookingReview.findById(reviewId).populate(
		'booking hotel customer'
	);
	if (!review) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found review with id ${reviewId}`
		);
	}

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		// update hotel's bookingRating
		if (updateBody.rating) {
			const hotel = await Hotel.findById(review.hotel);
			hotel.bookingRating.allValue += updateBody.rating - review.rating;
			hotel.bookingRating.avgValue =
				hotel.bookingRating.allValue / hotel.bookingRating.amount;
			await hotel.save(opts);

			const oldRating = review.rating;
			const month = review.updatedAt.getMonth() + 1;
			await bookingRatingReport(
				review.hotel,
				oldRating,
				updateBody.rating,
				month + 1
			);
		}

		Object.assign(review, updateBody);
		await review.save(opts);

		await session.commitTransaction();
		session.endSession();
		return review;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

module.exports = {
	createReview,
	findById,
	findByHotelId,
	updateById,
};
