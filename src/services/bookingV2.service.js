const {
	Booking,
	Room,
	RoomType,
	Hotel,
	Customer,
	Voucher,
} = require('../models');
const httpStatus = require('http-status');
const ApiError = require('../utils/ApiError');
const { bookingReport } = require('./hotelReport1.service');
const { voucherStatus } = require('../customs/db/voucherStatus');
const mongoose = require('mongoose');
const _ = require('lodash');
const { bookingStatus } = require('../customs/db/bookingStatus');
const { roundUp } = require('../utils/round');

const createBooking = async (bookingBody) => {
	const session = await mongoose.startSession();
	session.startTransaction();

	try {
		const opts = { session };

		const room = await Room.findById(bookingBody.room);
		const roomType = await RoomType.findById(room.roomType);
		const customer = await Customer.findById(bookingBody.customer);

		const bookingDuration =
			(bookingBody.bookingEnd - bookingBody.bookingStart) / 86400000;
		let money = bookingDuration * roomType.price;

		if (bookingBody.voucher) {
			const voucher = await Voucher.findById(bookingBody.voucher);
			if (!voucher) {
				throw new ApiError(
					httpStatus.NOT_FOUND,
					`Not found voucher with id ${id}`
				);
			}
			if (voucher.status === voucherStatus.UNAVAILABLE) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher ${voucher._id} is now unavailable.`
				);
			}
			if (!customer.voucher.includes(voucher._id)) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Customer ${customer.name} cannot use voucher '${voucher._id}'`
				);
			}
			if (voucher.roomType.toString() !== roomType._id.toString()) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is unavailable for roomType '${roomType.name}'.`
				);
			}
			if (
				bookingBody.bookingEnd < voucher.startDate ||
				bookingBody.bookingEnd > voucher.endDate
			) {
				throw new ApiError(
					httpStatus.BAD_REQUEST,
					`Voucher '${voucher._id}' is unavailable'.`
				);
			}

			money = (money * (100 - voucher.discount)) / 100;

			voucher.usingCustomers.push(customer._id);
			await voucher.save(opts);

			const voucherArr = customer.voucher.filter(
				(item) => item.toString() != voucher._id.toString()
			);
			customer.voucher = voucherArr;
			await customer.save(opts);
		}

		bookingBody.totalMoney = roundUp(money);
		const booking = await Booking.create([bookingBody], opts);

		room.bookings.push(booking[0]._id);
		await room.save(opts);

		await session.commitTransaction();
		session.endSession();
		return booking[0];
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const paymentBooking = async (bookingId) => {
	const booking = await Booking.findById(bookingId);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id ${bookingId}`
		);
	}

	if (booking.isPaid == true) {
		throw new ApiError(
			httpStatus.BAD_REQUEST,
			`This booking is already paid.`
		);
	}

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const room = await Room.findById(booking.room);
		const bookingArr = room.bookings.filter(
			(item) => item.toString() !== bookingId.toString()
		);
		room.bookings = bookingArr;
		await room.save(opts);

		booking.bookingStatus = bookingStatus.COMPLETED;
		booking.isPaid = true;
		await booking.save(opts);

		await bookingReport(booking.hotel, booking, room.roomType);

		await session.commitTransaction();
		session.endSession();
		return booking;
	} catch (e) {
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const getBookingById = async (bookingId) => {
	const booking = await Booking.findById(bookingId)
		.populate('customer voucher')
		.populate({ path: 'room', populate: { path: 'roomType' } });
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id ${bookingId}`
		);
	}
	return booking;
};

const cancelBookingById = async (bookingId) => {
	const booking = await Booking.findById(bookingId);
	if (!booking) {
		throw new ApiError(
			httpStatus.NOT_FOUND,
			`Not found booking with id '${bookingId}'`
		);
	}

	const session = await mongoose.startSession();
	session.startTransaction();
	try {
		const opts = { session };

		const room = await Room.findById(booking.room);
		const arr = room.bookings.filter((item) => item != bookingId);
		room.bookings = arr;
		await room.save(opts);

		booking.bookingStatus = bookingStatus.CANCELLED;
		await booking.save(opts);

		if (booking.voucher) {
			const voucher = await Voucher.findById(booking.voucher);
			const customer = await Customer.findById(booking.customer);

			customer.voucher.push(voucher._id);
			await customer.save(opts);

			const usingCustomerArr = voucher.usingCustomers.filter(
				(item) => item.toString() != customer._id.toString()
			);
			voucher.usingCustomers = usingCustomerArr;
			await voucher.save(opts);
		}

		await session.commitTransaction();
		session.endSession();
		return true;
	} catch (e) {
		// If an error occurred, abort the whole transaction and
		// undo any changes that might have happened
		await session.abortTransaction();
		session.endSession();
		throw new Error(e);
	}
};

const getAllBookingByHoelId = async (hotelId) => {
	const bookings = await Booking.find({ hotel: hotelId })
		.populate('customer voucher')
		.populate({ path: 'room', populate: { path: 'roomType' } });
	return bookings;
};

module.exports = {
	createBooking,
	paymentBooking,
	getBookingById,
	cancelBookingById,
	getAllBookingByHoelId,
};
