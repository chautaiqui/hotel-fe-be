const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const validator = require("validator");

const { accountStatus, accountStatusArr } = require("../customs/db/accStatus");
const {
  documentStatus,
  documentStatusArr,
} = require("../customs/db/documentStatus");
const { defaultImg } = require("../customs/db/cloudinaryDefault");

const adminSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email!");
        }
      },
    },
    password: {
      type: String,
      trim: true,
      required: true,
      default: "aaaa4444",
      validate(value) {
        if (value.length < 8) {
          throw new Error("Password must be at least 8 characters");
        }

        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error(
            "Password must contain at least one letter and one number"
          );
        }
      },
    },
    name: {
      type: String,
      trim: true,
      required: true,
    },
    img: {
      type: String,
      trim: true,
      default: defaultImg,
    },
    birthday: {
      type: Date,
      required: true,
    },
    phone: {
      type: String,
      trim: true,
      required: true,
    },
    address: {
      type: String,
      trim: true,
      required: true,
    },
    status: {
      type: String,
      trim: true,
      enum: accountStatusArr,
      default: accountStatus.AVAILABLE,
      validate(value) {
        if (!accountStatusArr.includes(value)) {
          throw new Error(`Invalid account status '${value}'.`);
        }
      },
    },
    docStatus: {
      type: String,
      trim: true,
      enum: documentStatusArr,
      default: documentStatus.AVAILABLE,
      validate(value) {
        if (!documentStatusArr.includes(value)) {
          throw new Error(`Invalid document status '${value}'.`);
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

/**
 * check if input password is match with admin's password
 * @param {string} password
 * @returns {Promise<boolean>}
 */
adminSchema.methods.isPasswordMatch = async function (password) {
  const admin = this;
  return await bcrypt.compare(password, admin.password);
  // return password == admin.password;
};

/**
 * pre save hook
 */
adminSchema.pre("save", async function () {
  const admin = this;

  //hash password
  if (admin.isModified("password")) {
    admin.password = await bcrypt.hash(admin.password, 8);
  }
});

const Admin = mongoose.model("Admin", adminSchema);

module.exports = Admin;
