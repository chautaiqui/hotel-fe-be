const mongoose = require('mongoose');
const {
	documentStatus,
	documentStatusArr,
} = require('../customs/db/documentStatus');

const bookingReviewSchema = new mongoose.Schema(
	{
		customer: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Customer',
			required: true,
		},
		booking: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Booking',
			required: true,
			unique: true,
		},
		hotel: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Hotel',
			required: true,
		},
		rating: {
			type: Number,
			min: 0,
			max: 5,
			required: true,
		},
		review: {
			type: String,
			trim: true,
			default: 'Booking review.',
		},
		docStatus: {
			type: String,
			trim: true,
			enum: documentStatusArr,
			default: documentStatus.AVAILABLE,
			validate(value) {
				if (!documentStatusArr.includes(value)) {
					throw new Error(`Invalid document status '${value}'.`);
				}
			},
		},
	},
	{
		timestamps: true,
	}
);

const BookingReviewModel = mongoose.model('BookingReview', bookingReviewSchema);

module.exports = BookingReviewModel;
