const mongoose = require("mongoose");
const { documentStatus, documentStatusArr } = require("../customs/db/documentStatus");

const blogSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
            trim: true
        },
        img: {
            type: String,
            trim: true,
            default: ""
        },
        content: {
            type: String,
            required: true,
            trim: true,
        },
        docStatus: {
            type: String,
            trim: true,
            enum: documentStatusArr,
            default: documentStatus.AVAILABLE,
            validate(value) {
                if (!documentStatusArr.includes(value)) {
                    throw new Error(`Invalid document status '${value}'.`);
                }
            }
        },
    }, 
    {
        timestamps: true
    }
);

const BlogModel = mongoose.model('Blog', blogSchema);

module.exports = BlogModel;