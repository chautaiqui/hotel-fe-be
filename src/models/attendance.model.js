const mongoose = require("mongoose");
const {
  documentStatus,
  documentStatusArr,
} = require("../customs/db/documentStatus");

const attendanceSchema = new mongoose.Schema(
  {
    employee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Employee",
      required: true,
    },
    isAttendance: {
      type: Boolean,
      required: true,
      default: true,
    },
    shifts: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "HotelWorkingShift",
      required: true,
    },
    docStatus: {
      type: String,
      trim: true,
      enum: documentStatusArr,
      default: documentStatus.AVAILABLE,
      validate(value) {
        if (!documentStatusArr.includes(value)) {
          throw new Error(`Invalid document status '${value}'.`);
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

const Attendance = mongoose.model("Attandance", attendanceSchema);
module.exports = Attendance;
