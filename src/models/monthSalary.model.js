const mongoose = require("mongoose");
const {
  documentStatus,
  documentStatusArr,
} = require("../customs/db/documentStatus");

const monthSalarySchema = new mongoose.Schema(
  {
    employee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Employee",
      required: true,
    },
    salary: {
      type: Number,
      required: true,
      default: 0,
    },
    bonus: {
      type: Number,
      default: 0,
    },
    comment: {
      type: String,
      default: "",
    },
    docStatus: {
      type: String,
      trim: true,
      enum: documentStatusArr,
      default: documentStatus.AVAILABLE,
      validate(value) {
        if (!documentStatusArr.includes(value)) {
          throw new Error(`Invalid document status '${value}'.`);
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

const MonthSalary = mongoose.model("MonthSalary", monthSalarySchema);
module.exports = MonthSalary;
