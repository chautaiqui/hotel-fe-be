const mongoose = require('mongoose');
const {
	documentStatus,
	documentStatusArr,
} = require('../customs/db/documentStatus');

const facilitySchema = new mongoose.Schema(
	{
		hotel: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Hotel',
			required: true,
		},
		type: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'FacilityType',
			required: true,
		},
		name: {
			type: String,
			trim: true,
			required: true,
		},
		amount: {
			type: Number,
			min: 0,
			required: true,
			validate(value) {
				if (value < 0) {
					throw new Error('Facility amount cannot be lower than 0.');
				}
			},
		},
		availableAmount: {
			type: Number,
			min: 0,
			required: true,
			default: function () {
				return this.amount;
			},
			validate(value) {
				if (value > this.amount) {
					throw new Error(
						'Facility available amount cannot be higher than amount.'
					);
				}
				if (value < 0) {
					throw new Error(
						'Facility available amount cannot be lower than 0.'
					);
				}
			},
		},
		docStatus: {
			type: String,
			trim: true,
			enum: documentStatusArr,
			default: documentStatus.AVAILABLE,
			validate(value) {
				if (!documentStatusArr.includes(value)) {
					throw new Error(`Invalid document status '${value}'.`);
				}
			},
		},
	},
	{
		timestamps: true,
	}
);

facilitySchema.path('name').validate(async function () {
	const { type, name } = this;
	if (this.isModified('name')) {
		const count = await mongoose
			.model('Facility', facilitySchema)
			.countDocuments({ type: type, name: name });
		if (count > 0)
			throw new Error(
				`Facility '${name}' is already exist in facility type id '${type}'.`
			);
	}
	return true;
});

const Facility = mongoose.model('Facility', facilitySchema);
module.exports = Facility;
