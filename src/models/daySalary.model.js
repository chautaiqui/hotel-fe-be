const mongoose = require("mongoose");
const {
  documentStatus,
  documentStatusArr,
} = require("../customs/db/documentStatus");

const daySalarySchema = new mongoose.Schema(
  {
    employee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Employee",
      required: true,
    },
    monthSalary: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "MonthSalary",
    },
    date: {
      type: Date,
      required: true,
    },
    attendance: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Attendance",
      required: true,
    },
    baseSalary: {
      type: Number,
      required: true,
    },
    bonus: {
      type: Number,
      required: true,
      default: 0,
    },
    reasonForBonus: {
      type: String,
      trim: true,
      default: "",
    },
    totalSalary: {
      type: Number,
      required: true,
    },
    forMonthSalary: {
      type: Boolean,
      default: false,
    },
    docStatus: {
      type: String,
      trim: true,
      enum: documentStatusArr,
      default: documentStatus.AVAILABLE,
      validate(value) {
        if (!documentStatusArr.includes(value)) {
          throw new Error(`Invalid document status '${value}'.`);
        }
      },
    },
  },
  {
    timestamps: true,
  }
);

const DaySalary = mongoose.model("DaySalary", daySalarySchema);
module.exports = DaySalary;
