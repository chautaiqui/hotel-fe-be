const mongoose = require('mongoose');

const {
	documentStatus,
	documentStatusArr,
} = require('../customs/db/documentStatus');
const {
	bookingStatus,
	bookingStatusArr,
} = require('../customs/db/bookingStatus');
const { string } = require('joi');

const bookingSchema = new mongoose.Schema(
	{
		hotel: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Hotel',
			required: true,
		},
		room: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Room',
			required: true,
		},
		customer: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Customer',
			// required: true,
		},
		name: {
			type: String,
			trim: true,
		},
		bookingStart: {
			type: Date,
			required: true,
		},
		bookingEnd: {
			type: Date,
			required: true,
		},
		voucher: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Voucher',
		},
		totalMoney: {
			type: Number,
			min: 0,
			default: 0,
		},
		bookingStatus: {
			type: String,
			trim: true,
			default: bookingStatus.PENDING,
			enum: bookingStatusArr,
			validate(value) {
				if (!bookingStatusArr.includes(value)) {
					throw new Error(`Invalid booking status '${value}'.`);
				}
			},
		},
		isPaid: {
			type: Boolean,
			default: false,
		},
		docStatus: {
			type: String,
			trim: true,
			enum: documentStatusArr,
			default: documentStatus.AVAILABLE,
			validate(value) {
				if (!documentStatusArr.includes(value)) {
					throw new Error(`Invalid document status '${value}'.`);
				}
			},
		},
	},
	{
		timestamps: true,
	}
);

const Booking = mongoose.model('Booking', bookingSchema);
module.exports = Booking;
