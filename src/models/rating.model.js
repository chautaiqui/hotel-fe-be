const mongoose = require('mongoose');
const {
	documentStatus,
	documentStatusArr,
} = require('../customs/db/documentStatus');

const ratingSchema = new mongoose.Schema(
	{
		customer: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Customer',
			required: true,
		},
		hotel: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Hotel',
			required: true,
		},
		rating: {
			type: Number,
			min: 0,
			max: 5,
			required: true,
		},
		comment: {
			type: String,
			trim: true,
			default: 'comment',
		},
		docStatus: {
			type: String,
			trim: true,
			enum: documentStatusArr,
			default: documentStatus.AVAILABLE,
			validate(value) {
				if (!documentStatusArr.includes(value)) {
					throw new Error(`Invalid document status '${value}'.`);
				}
			},
		},
	},
	{
		timestamps: true,
	}
);

const RatingModel = mongoose.model('Rating', ratingSchema);

module.exports = RatingModel;
