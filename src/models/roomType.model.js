const mongoose = require("mongoose");

const { documentStatus, documentStatusArr } = require("../customs/db/documentStatus");

// const facilitySchema = new mongoose.Schema(
//     {
//         type: {
//             type: String,
//             trim: true,
//             required: true,
//         },
//         amount: {
//             type: Number,
//             min: 0,
//             default: 0,
//         },
//     },
// );

const roomTypeSchema = new mongoose.Schema(
    {
        hotel: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Hotel"
        },
        name: {
            type: String,
            required: true,
            trim: true,
        },
        capacity: {
            type: Number,
            required: true,
            min: 1,
            default: 1,
        },
        price: {
            type: Number,
            required: true,
            min: 1000,
            default: 1000,
        },
        // facilities: {
        //     type: [facilitySchema],
        //     required: true,
        //     default: [],
        // },
        docStatus: {
            type: String,
            trim: true,
            enum: documentStatusArr,
            default: documentStatus.AVAILABLE,
            validate(value) {
                if (!documentStatusArr.includes(value)) {
                    throw new Error(`Invalid document status '${value}'.`);
                }
            }
        },
    },
    {
        timestamps: true
    }
);

roomTypeSchema.path('name').validate(async function() {
    const { hotel, name } = this;
    if (this.isModified('name')){
        const count = await mongoose.model("RoomType", roomTypeSchema).countDocuments({ hotel: hotel, name: name });
        if (count > 0) throw new Error(`Room type '${name}' is already exist in hotel id '${hotel}'.`);
    }
    return true;
});

const RoomType = mongoose.model("RoomType", roomTypeSchema);

module.exports = RoomType;