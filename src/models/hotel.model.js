const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const {
	documentStatus,
	documentStatusArr,
} = require('../customs/db/documentStatus');

const hotelSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
			trim: true,
			unique: true,
		},
		manager: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'Employee',
		},
		capacity: {
			type: Number,
			default: 0,
		},
		imgs: {
			type: [String],
			trim: true,
			default: [],
		},
		description: {
			type: String,
			trim: true,
			default: 'Hotel description.',
		},
		phone: {
			type: String,
			trim: true,
			required: true,
		},
		address: {
			type: String,
			trim: true,
		},
		street: {
			type: String,
			trim: true,
		},
		ward: {
			type: String,
			trim: true,
		},
		district: {
			type: String,
			trim: true,
		},
		province: {
			type: String,
			trim: true,
		},
		timeIn: {
			type: Date,
			default: Date.now(),
		},
		timeOut: {
			type: Date,
			default: Date.now(),
		},
		// averagePrice: {
		// 	type: Number,
		// 	default: 0,
		// },
		averagePrice: {
			avgValue: {
				type: Number,
				min: 0,
				default: 0,
			},
			allValue: {
				type: Number,
				min: 0,
				default: 0,
			},
			amount: {
				type: Number,
				min: 0,
				default: 0,
			},
		},
		rated: {
			avgValue: {
				type: Number,
				min: 0,
				max: 5,
				default: 0,
			},
			allValue: {
				type: Number,
				min: 0,
				default: 0,
			},
			amount: {
				type: Number,
				min: 0,
				default: 0,
			},
		},
		bookingRating: {
			avgValue: {
				type: Number,
				min: 0,
				max: 5,
				default: 0,
			},
			allValue: {
				type: Number,
				min: 0,
				default: 0,
			},
			amount: {
				type: Number,
				min: 0,
				default: 0,
			},
		},
		docStatus: {
			type: String,
			trim: true,
			enum: documentStatusArr,
			default: documentStatus.AVAILABLE,
			validate(value) {
				if (!documentStatusArr.includes(value)) {
					throw new Error(`Invalid document status '${value}'.`);
				}
			},
		},
	},
	{
		timestamps: true,
	}
);

hotelSchema.index({ province: 'text' });
hotelSchema.plugin(mongoosePaginate);

const Hotel = mongoose.model('Hotel', hotelSchema);

module.exports = Hotel;
