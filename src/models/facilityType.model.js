const mongoose = require("mongoose");
const { documentStatus, documentStatusArr } = require("../customs/db/documentStatus");

const facilityTypeSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: true,
        },
        hotel: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Hotel',
            required: true,
        },
        amount: {
            type: Number,
            min: 0,
            default: 0,
            required: true,
            validate(value) {
                if (value < 0) {
                    throw new Error("Facility type amount cannot be lower than 0.")
                }
            }
        },
        availableAmount: {
            type: Number,
            min: 0,
            required: true,
            default: 0,
            validate(value) {
                if (value > this.amount) {
                    throw new Error("Facility type available amount cannot be higher than amount.")
                }
                if (value < 0) {
                    throw new Error("Facility type available amount cannot be lower than 0.")
                }
            }
        },
        docStatus: {
            type: String,
            trim: true,
            enum: documentStatusArr,
            default: documentStatus.AVAILABLE,
            validate(value) {
                if (!documentStatusArr.includes(value)) {
                    throw new Error(`Invalid document status '${value}'.`);
                }
            }
        },
    },
    {
        timestamps: true,
    }
);

facilityTypeSchema.path("name").validate(async function() {
    const { hotel, name } = this;
    if (this.isModified('name')){
        const count = await mongoose.model("FacilityType", facilityTypeSchema).countDocuments({ hotel: hotel, name: name });
        if (count > 0) throw new Error(`Facility type '${name}' is already exist in hotel id '${hotel}'.`);
    }
    return true;
});

const FacilityType = mongoose.model("FacilityType", facilityTypeSchema);
module.exports = FacilityType;